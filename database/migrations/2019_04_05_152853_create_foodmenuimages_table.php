<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodmenuimagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foodmenuimages', function (Blueprint $table) {
            $table
                ->increments('fmi_id')
                ->comment('food menu image id, primary key, auto increment starts from 1');

            $table
                ->integer('fm_id')
                ->comment('food menu id, foreign key, comes from foodMenus.fm_id')
                ->default(0);

            $table
                ->text('fmi_image')
                ->comment('food menu image path, not nullable')
                ->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foodmenuimages');
    }
}
