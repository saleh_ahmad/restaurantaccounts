<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodmenucategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foodmenucategories', function (Blueprint $table) {
            $table
                ->increments('fmc_id')
                ->comment('food menu category id, primary key, auto increment starts from 1');

            $table
                ->string('fmc_name', 128)
                ->comment('food menu category name, not nullable');

            $table
                ->text('fmc_description')
                ->comment('food menu category description, nullable')
                ->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foodmenucategories');
    }
}
