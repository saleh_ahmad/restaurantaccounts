<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory', function (Blueprint $table) {
            $table
                ->bigIncrements('inv_id')
                ->comment('inventory id, primary key, auto increment starts from 1');

            $table
                ->integer('rm_id')
                ->comment('raw material id, foreign key, comes from  rawMaterials.rm_id');

            $table
                ->string('Inv_beginningQty', 128)
                ->comment('beginning qty of the raw material, nullable')
                ->nullable();

            $table
                ->string('Inv_orderedQty', 128)
                ->comment('ordered qty of the raw material, nullable')
                ->nullable();

            $table
                ->string('Inv_rerceivedQty', 128)
                ->comment('received qty of the raw material, nullable')
                ->nullable();

            $table
                ->string('inv_presentQty', 128)
                ->comment('present qty of the raw material (Inv_beginningQty + Inv_rerceivedQty), nullable')
                ->nullable();

            $table
                ->string('inv_endingQty', 128)
                ->comment('ending qty of the raw material (beginning qty for the next day), nullable')
                ->nullable();

            $table
                ->string('inv_usage', 128)
                ->comment('total usage qty of the day (inv_presentQty - inv_endingQty), nullable')
                ->nullable();

            $table
                ->date('inv_date')
                ->comment('date of the day');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory');
    }
}
