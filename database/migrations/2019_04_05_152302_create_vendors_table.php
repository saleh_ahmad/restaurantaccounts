<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table
                ->increments('v_id')
                ->comment('vendor id, primary key, auto increment starts from 1');

            $table
                ->string('v_name', 128)
                ->comment('vendor name i.e. Asha, Like foods');

            $table
                ->string('v_mobile', 20)
                ->comment('vendor mobile number, + is also allowed, must be unique')
                ->unique();

            $table
                ->text('v_address')
                ->comment('vendor\'s shop address, nullable')
                ->nullable();

            $table
                ->text('v_description')
                ->comment('short description about a vendor, nullable')
                ->nullable();

            $table
                ->decimal('v_totalBill', 12, 2)
                ->comment('vendor\'s lifetime bill')
                ->default(0.00);

            $table
                ->decimal('v_totalPaid', 12, 2)
                ->comment('total paid amount to the vendors')
                ->default(0.00);

            $table
                ->decimal('v_due', 12, 2)
                ->comment('vendor\'s total due amount')
                ->default(0.00);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
