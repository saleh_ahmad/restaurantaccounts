<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryorderdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventoryorderdetails', function (Blueprint $table) {
            $table
                ->bigIncrements('iod_id')
                ->comment('inventory order details id, primary key, auto increment id starts from 1');

            $table
                ->integer('rm_id')
                ->comment('raw materials id, foreign id, comes from rawMaterials.rm_id')
                ->default(0);

            $table
                ->decimal('iod_unitPrice', 12, 2)
                ->comment('inventory ordered raw material unit price, it may vary.')
                ->default(0.00);

            $table
                ->string('iod_orderedQuantity', 128)
                ->comment('quantity of the ordered raw material')
                ->nullable();

            $table
                ->string('iod_receivedQuantity', 128)
                ->comment('quantity of the received raw material')
                ->nullable();

            $table
                ->decimal('iod_totalPrice', 12, 2)
                ->comment('total price of the raw material (iod_unitPrice * iod_quantity)')
                ->default(0.00);

            $table
                ->bigInteger('iom_id')
                ->comment('inventory order master, foreign key, comes from inventoryOrderMaster.iom_id')
                ->default(0);

            $table
                ->text('iod_receivedRawMaterialImage')
                ->comment('image of the ordered raw material')
                ->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventoryorderdetails');
    }
}
