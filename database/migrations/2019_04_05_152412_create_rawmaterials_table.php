<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRawmaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rawmaterials', function (Blueprint $table) {
            $table
                ->increments('rm_id')
                ->comment('raw materials id, primary key, auto increment starts from 1');

            $table
                ->string('rm_name', 128)
                ->comment('raw material name i.e. oil, sugar, cheese, bun');

            $table
                ->text('rm_description')
                ->comment('description of the raw material')
                ->nullable();

            $table
                ->text('rm_image')
                ->comment('raw material\'s image')
                ->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rawmaterials');
    }
}
