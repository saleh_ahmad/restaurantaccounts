<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashboxtrnxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashboxtrnx', function (Blueprint $table) {
            $table
                ->increments('cbt_id')
                ->comment('cash box transaction id, primary key, auto increment starts from 1');

            $table
                ->tinyInteger('cbt_type')
                ->comment('type of the transaction, 1 for debit, 2 for credit')
                ->default(0);

            $table
                ->decimal('cbt_amount', 12, 2)
                ->comment('cash box transaction amount')
                ->default(0.00);

            $table
                ->text('cbt_purpose')
                ->comment('purpose of the transaction')
                ->nullable();

            $table
                ->integer('cbtc_id')
                ->comment('cash box transaction category id, foreign key, comes from cashBoxTrnxCategories.cbtc_id')
                ->default(0);

            $table
                ->integer('u_id')
                ->comment('user id who associated with the transaction, foreign key, comes from users.u_id')
                ->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashboxtrnx');
    }
}
