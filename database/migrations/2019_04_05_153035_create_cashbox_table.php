<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashboxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashbox', function (Blueprint $table) {
            $table
                ->increments('cb_id')
                ->comment('cash box id, primary key, auto increment starts from 1');

            $table
                ->decimal('cb_amountInTheCashBox', 12, 2)
                ->comment('amount found in the cash box')
                ->default(0.00);

            $table
                ->decimal('cb_totalDebit', 12, 2)
                ->comment('total earning of the day (cash-in + food order)')
                ->default(0.00);

            $table
                ->decimal('cb_totalCredit', 12, 2)
                ->comment('total costs of the day (vendor\'s bill + other costs + cash out + salary paid)')
                ->default(0.00);

            $table
                ->decimal('cb_expectedAmount', 12, 2)
                ->comment('cb_totalDebit - cb_totalCredit')
                ->default(0.00);

            $table
                ->date('cb_date')
                ->comment('date of the day, not nullable');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashbox');
    }
}
