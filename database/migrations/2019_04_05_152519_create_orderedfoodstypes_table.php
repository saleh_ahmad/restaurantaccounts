<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderedfoodstypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderedfoodstypes', function (Blueprint $table) {
            $table
                ->increments('oft_id')
                ->comment('ordered food type id, primary key, auto incerement starts from 1');

            $table
                ->string('oft_name', 50)
                ->comment('order food type name i.e. shop, food panda, pathao');

            $table
                ->text('oft_description')
                ->comment('short or long long description of the type')
                ->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderedfoodstypes');
    }
}
