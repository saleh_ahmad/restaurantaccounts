<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderedfoodsmasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderedfoodsmaster', function (Blueprint $table) {
            $table
                ->bigIncrements('ofm_id')
                ->comment('ordered food master id, primary key, suto increment starts from 1');

            $table
                ->integer('oft_id')
                ->comment('ordered food type id, foreign key, comes from orderedFoodsTypes.oft_id')
                ->default(0);

            $table
                ->integer('s_id')
                ->comment('stuff id who take the order, foreign key, comes from staff.s_id')
                ->default(0);

            $table
                ->decimal('ofm_totalPrice', 12, 2)
                ->comment('total price of the ordered foods')
                ->default(0.00);

            $table
                ->decimal('ofm_discountPrice', 12, 2)
                ->comment('discount price of the ordered foods')
                ->default(0.00);

            $table
                ->decimal('ofm_finalPrice', 12, 2)
                ->comment('final price of the ordered foods (totalPrice - DiscountPrice)')
                ->default(0.00);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderedfoodsmaster');
    }
}
