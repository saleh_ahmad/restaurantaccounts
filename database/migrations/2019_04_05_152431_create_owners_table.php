<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owners', function (Blueprint $table) {
            $table
                ->increments('o_id')
                ->comment('owner id, primary key, auto increent starts from 1');

            $table
                ->integer('u_id')
                ->comment('user id, foreign key, comes from users.u_id')
                ->default(0);

            $table
                ->decimal('o_totalDue', 12, 2)
                ->comment('total due payment of the owner, can\'t be negative')
                ->default(0.00);

            $table
                ->decimal('o_totalGiven', 12, 2)
                ->comment('total given money of the owner, can\'t be negative')
                ->default(0.00);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owners');
    }
}
