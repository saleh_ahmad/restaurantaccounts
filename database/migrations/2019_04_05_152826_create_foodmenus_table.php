<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodmenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foodmenus', function (Blueprint $table) {
            $table
                ->increments('fm_id')
                ->comment('food menu id, primary key, auto increment starts from 1');

            $table
                ->string('fm_name', 128)
                ->comment('food menu name, not nullable');

            $table
                ->text('fm_description')
                ->comment('food menu description, null when no description added')
                ->nullable();

            $table
                ->integer('fmc_id')
                ->comment('food menu category id, foreign key, comes from foodMenuCategories.fmc_id')
                ->default(0);

            $table
                ->decimal('fm_makingCost', 8, 2)
                ->comment('making cost of the food menu')
                ->default(0.00);

            $table
                ->decimal('fm_sellingPrice', 8, 2)
                ->comment('selling cost of the food menu')
                ->default(0.00);

            $table
                ->decimal('fm_profit', 8, 2)
                ->comment('profit over the food menu (fm_sellingPrice - fm_makingCost)')
                ->default(0.00);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foodmenus');
    }
}
