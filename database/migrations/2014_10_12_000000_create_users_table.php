<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table
                ->bigIncrements('u_id')
                ->comment('user auto generated id, primary key, auto incremented column starting from 1.');

            $table
                ->string('u_name', 128)
                ->comment('user name, not nullable');

            $table
                ->string('u_mobile', 20)
                ->comment('user mobile number, not nullable, must be unique as it will use as an unique identifier in the application.')
                ->unique();

            $table
                ->string('email', 180)
                ->nullable()
                ->unique();

            $table->timestamp('email_verified_at')->nullable();
            $table->string('u_password', 256)->comment('user password, not nullable, encrypted from the application.');
            $table->text('u_address')->comment('user address, null if user won\'t give any address')->nullable();
            $table->text('u_image')->comment('user profile image. name can be too big, null if user won\'t give any image of him/her.')->nullable();
            $table->integer('ur_id')->comment('user role id, foreign key, comes from userRoles.ur_id')->default(0);

            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
