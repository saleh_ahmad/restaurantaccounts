<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderedfoodsdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderedfoodsdetails', function (Blueprint $table) {
            $table
                ->bigIncrements('ofd_id')
                ->comment('ordered food details id, primary key, auto increment starts from 1');

            $table
                ->bigInteger('ofm_id')
                ->comment('ofdered food master id, foreign key, comes from orderedFoodsMaster.ofm_id')
                ->default(0);

            $table
                ->integer('fm_id')
                ->comment('food menu id, foreign key, comes from foodMenus.fm_id')
                ->default(0);

            $table
                ->integer('ofd_qty')
                ->comment('order food menu quantity')
                ->default(0);

            $table
                ->decimal('ofd_price', 12, 2)
                ->comment('total price of the ordered food menu (unit price of the food menu * ofd_qty)')
                ->default(0.00);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderedfoodsdetails');
    }
}
