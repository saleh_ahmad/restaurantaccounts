<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashboxtrnxcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashboxtrnxcategories', function (Blueprint $table) {
            $table
                ->increments('cbtc_id')
                ->comment('cash box transaction categories id, primary key, auto increment starts from 1');

            $table
                ->string('cbtc_name', 128)
                ->comment('cash box transaction category name i.e. cash-in, food order');

            $table
                ->text('cbtc_description')
                ->comment('cash box transaction category description')
                ->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashboxtrnxcategories');
    }
}
