<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalarytransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salarytransaction', function (Blueprint $table) {
            $table
                ->bigIncrements('st_id')
                ->comment('salary transaction id, primary key, Auto increment starts from 1');

            $table
                ->decimal('st_givenSalary', 12, 2)
                ->comment('given salary to the staff by a owner')
                ->default(0.00);

            $table
                ->decimal('st_totalPaidSalary', 12, 2)
                ->comment('total paid salary in that month to the staff by a owners')
                ->default(0.00);

            $table
                ->decimal('st_totalDueSalary', 12, 2)
                ->comment('total due salary in that month to the staff by the owners')
                ->default(0.00);

            $table
                ->integer('s_id')
                ->comment('staff id, foreign key, comes from staff.s_id')
                ->default(0);

            $table
                ->integer('o_id')
                ->comment('owner id, foreign key, comes from owners.o_id')
                ->default(0);

            $table
                ->date('st_month')
                ->comment('month of the given salary to the staff by a owner, not nullable');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salarytransaction');
    }
}
