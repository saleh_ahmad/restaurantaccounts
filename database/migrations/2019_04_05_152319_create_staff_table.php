<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table
                ->increments('s_id')
                ->comment('staff id, primary key, auto increment starts from 1');

            $table
                ->integer('u_id')
                ->comment('user id, foreign key, comes from users.u_id')
                ->default(0);

            $table
                ->decimal('s_salary', 12, 2)
                ->comment('staff salary from the owners, can\'t be negative')
                ->default(0.00);

            $table
                ->date('s_joiningDate')
                ->comment('staff joining date, null if no joining date will input')
                ->nullable();

            $table
                ->date('s_endingDate')
                ->comment('staff resigning date, null if staff is currently working')
                ->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
