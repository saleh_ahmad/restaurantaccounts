<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryordermasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventoryordermaster', function (Blueprint $table) {
            $table
                ->bigIncrements('iom_id')
                ->comment('inventory order master id, primary key, auto increment starts from 1');

            $table
                ->integer('v_id')
                ->comment('vendor id, foreign key, comes from vendors.v_id')
                ->default(0);

            $table
                ->text('iom_description')
                ->comment('description of the order')
                ->nullable();

            $table
                ->decimal('iom_bill', 12, 2)
                ->comment('Total Bill of the order')
                ->default(0.00);

            $table
                ->text('iom_billCopyImage')
                ->comment('Bill copy image of the order')
                ->nullable();

            $table
                ->decimal('iom_paid', 12, 2)
                ->comment('total paid of the bill')
                ->default(0.00);

            $table
                ->date('iom_date')
                ->comment('date of the order');

            $table
                ->integer('iom_orderedUserId')
                ->comment('user who order the raw materials')
                ->default(0);

            $table
                ->integer('iom_receivedUserId')
                ->comment('user who receive the raw materials')
                ->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventoryordermaster');
    }
}
