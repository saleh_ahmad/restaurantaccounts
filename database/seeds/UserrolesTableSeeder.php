<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserrolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('userroles')->insert([
            [
                'ur_name'        => 'Owner',
                'ur_description' => 'Owner of the restaurant.',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'ur_name'        => 'Manager',
                'ur_description' => 'Restaurant manager',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'ur_name'        => 'Chef',
                'ur_description' => 'Restaurant chef.',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'ur_name'        => 'Cleaner',
                'ur_description' => 'Restaurant Cleaner.',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ]
        ]);
    }
}
