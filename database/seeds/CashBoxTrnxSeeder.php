<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CashBoxTrnxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cashboxtrnx')->insert([
            [
                'cbt_type'    => 1,
                'cbt_amount'  => 400.00,
                'cbt_purpose' => 'Customer Order',
                'cbtc_id'     => 3,
                'u_id'        => 3,
                'updated_at'  => Carbon::now(),
                'created_at'  => Carbon::now()
            ],
            [
                'cbt_type'    => 1,
                'cbt_amount'  => 80000.00,
                'cbt_purpose' => 'Investment',
                'cbtc_id'     => 1,
                'u_id'        => 1,
                'updated_at'  => Carbon::now(),
                'created_at'  => Carbon::now()
            ],
            [
                'cbt_type'    => 2,
                'cbt_amount'  => 20000.00,
                'cbt_purpose' => 'Give salary to the Manager',
                'cbtc_id'     => 6,
                'u_id'        => 1,
                'updated_at'  => Carbon::now(),
                'created_at'  => Carbon::now()
            ],
            [
                'cbt_type'    => 2,
                'cbt_amount'  => 15000.00,
                'cbt_purpose' => 'Give salary to the Chef',
                'cbtc_id'     => 6,
                'u_id'        => 1,
                'updated_at'  => Carbon::now(),
                'created_at'  => Carbon::now()
            ],
            [
                'cbt_type'    => 2,
                'cbt_amount'  => 5000.00,
                'cbt_purpose' => 'Give salary to the Cleaner',
                'cbtc_id'     => 6,
                'u_id'        => 1,
                'updated_at'  => Carbon::now(),
                'created_at'  => Carbon::now()
            ],
            [
                'cbt_type'    => 2,
                'cbt_amount'  => 4000.00,
                'cbt_purpose' => 'Bill Paid (Asha)',
                'cbtc_id'     => 5,
                'u_id'        => 3,
                'updated_at'  => Carbon::now(),
                'created_at'  => Carbon::now()
            ]
        ]);
    }
}
