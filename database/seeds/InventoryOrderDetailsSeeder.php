<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class InventoryOrderDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inventoryorderdetails')->insert([
            [
                'rm_id'                        => 1,
                'iod_unitPrice'                => 400.00,
                'iod_orderedQuantity'          => '10kg',
                'iod_receivedQuantity'         => '10kg',
                'iod_totalPrice'               => 4000.00,
                'iom_id'                       => 1,
                'iod_receivedRawMaterialImage' => 'assets/images/receivedItems/27.03.2019.jpg',
                'updated_at'                   => Carbon::now(),
                'created_at'                   => Carbon::now()
            ]
        ]);
    }
}
