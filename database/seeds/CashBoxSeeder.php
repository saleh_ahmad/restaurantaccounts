<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CashBoxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cashbox')->insert([
            [
                'cb_amountInTheCashBox' => 400.00,
                'cb_totalDebit'         => 80400.00,
                'cb_totalCredit'        => 44000.00,
                'cb_expectedAmount'     => 40400.00,
                'cb_date'               => '2019-04-16',
                'updated_at'            => Carbon::now(),
                'created_at'            => Carbon::now()
            ]
        ]);
    }
}
