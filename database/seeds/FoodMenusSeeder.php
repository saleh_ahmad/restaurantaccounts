<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class FoodMenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('foodmenus')->insert([
            [
                'fm_name'         => 'Student Meal 01',
                'fm_description'  => 'Fried Rice, Chicken Fry, Gravy vegetables',
                'fmc_id'          => 3,
                'fm_makingCost'   => 80.00,
                'fm_sellingPrice' => 140.00,
                'fm_profit'       => 60.00,
                'updated_at'      => Carbon::now(),
                'created_at'      => Carbon::now()
            ],
            [
                'fm_name'         => 'Student Meal 02',
                'fm_description'  => 'Fried Rice, Spicy Chicken Wings (2 pc), Gravy Vegetables',
                'fmc_id'          => 3,
                'fm_makingCost'   => 120.00,
                'fm_sellingPrice' => 170.00,
                'fm_profit'       => 50.00,
                'updated_at'      => Carbon::now(),
                'created_at'      => Carbon::now()
            ]
        ]);
    }
}
