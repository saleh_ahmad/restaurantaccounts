<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inventory')->insert([
            [
                'rm_id'            => 1,
                'Inv_beginningQty' => '0kg',
                'Inv_orderedQty'   => '10kg',
                'Inv_rerceivedQty' => '10kg',
                'inv_presentQty'   => '10kg',
                'inv_endingQty'    => '3kg',
                'inv_usage'        => '7kg',
                'inv_date'         => '2019-04-16',
                'updated_at'       => Carbon::now(),
                'created_at'       => Carbon::now()
            ]
        ]);
    }
}
