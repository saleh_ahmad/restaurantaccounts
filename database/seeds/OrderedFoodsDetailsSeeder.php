<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OrderedFoodsDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orderedfoodsdetails')->insert([
            [
                'ofm_id'     => 1,
                'fm_id'      => 1,
                'ofd_qty'    => 3,
                'ofd_price'  => 420.00,
                'updated_at' => Carbon::now(),
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
