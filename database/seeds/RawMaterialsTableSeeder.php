<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RawMaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rawmaterials')->insert([
            [
                'rm_name'        => 'Boneless Chicken',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/01.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Chicken Leg',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/02.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Chicken Wings',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/03.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Whole Chicken',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/04.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Alu (Diamond)',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/05.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Piyaj (Indian)',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/06.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Roshun',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/07.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Ada',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/08.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Thai Ada',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/09.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Moyda',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/10.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Polao Rice',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/11.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Soyabean Oil',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/12.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Salt',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/13.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Sugar',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/14.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Thai Oyster Sauce',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/15.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Dragon Noodles',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/16.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ],
            [
                'rm_name'        => 'Wheep Cream',
                'rm_description' => null,
                'rm_image'       => 'assets/images/rawmaterials/17.jpg',
                'updated_at'     => Carbon::now(),
                'created_at'     => Carbon::now()
            ]
        ]);
    }
}
