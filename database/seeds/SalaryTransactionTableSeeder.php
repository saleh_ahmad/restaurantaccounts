<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SalaryTransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('salarytransaction')->insert([
            [
                'st_givenSalary'     => 20000.00,
                'st_totalPaidSalary' => 20000.00,
                'st_totalDueSalary'  => 0,
                's_id'               => 1,
                'o_id'               => 1,
                'st_month'           => '2019-03-01',
                'updated_at'         => Carbon::now(),
                'created_at'         => Carbon::now()
            ],
            [
                'st_givenSalary'     => 15000.00,
                'st_totalPaidSalary' => 15000.00,
                'st_totalDueSalary'  => 0,
                's_id'               => 2,
                'o_id'               => 1,
                'st_month'           => '2019-03-01',
                'updated_at'         => Carbon::now(),
                'created_at'         => Carbon::now()
            ],
            [
                'st_givenSalary'     => 5000.00,
                'st_totalPaidSalary' => 5000.00,
                'st_totalDueSalary'  => 0,
                's_id'               => 3,
                'o_id'               => 1,
                'st_month'           => '2019-03-01',
                'updated_at'         => Carbon::now(),
                'created_at'         => Carbon::now()
            ]
        ]);
    }
}
