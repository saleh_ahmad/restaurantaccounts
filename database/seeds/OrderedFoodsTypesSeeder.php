<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OrderedFoodsTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orderedfoodstypes')->insert([
            [
                'oft_name'        => 'Shop',
                'oft_description' => null,
                'updated_at'      => Carbon::now(),
                'created_at'      => Carbon::now()
            ],
            [
                'oft_name'        => 'Food panda',
                'oft_description' => null,
                'updated_at'      => Carbon::now(),
                'created_at'      => Carbon::now()
            ],
            [
                'oft_name'        => 'Hungry Naki',
                'oft_description' => null,
                'updated_at'      => Carbon::now(),
                'created_at'      => Carbon::now()
            ],
            [
                'oft_name'        => 'Pathao',
                'oft_description' => null,
                'updated_at'      => Carbon::now(),
                'created_at'      => Carbon::now()
            ]
        ]);
    }
}
