<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class FoodMenuImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('foodmenuimages')->insert([
            [
                'fm_id'      => 1,
                'fmi_image'  => 'assets/images/foodmenu/studentMenu01.jpg',
                'updated_at' => Carbon::now(),
                'created_at' => Carbon::now()
            ],
            [
                'fm_id'      => 3,
                'fmi_image'  => 'assets/images/foodmenu/studentMenu02.jpg',
                'updated_at' => Carbon::now(),
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
