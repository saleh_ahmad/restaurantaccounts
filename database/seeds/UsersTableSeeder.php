<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'u_name'            => 'Hasan Mahmud Maruf',
                'u_mobile'          => '+8801670445591',
                'email'             => null,
                'email_verified_at' => null,
                'u_password'        => '$2y$10$t8YCD9EbQIn8zoCboLpCrOLCkvS0/YGFKKa/RUoWGx43U8//eT5zi',
                'u_address'         => '08/161, 12/E, Mirpur, Dhaka - 1216',
                'u_image'           => 'storage/app/users/oyon.jpg',
                'ur_id'             => 1,
                'updated_at'        => Carbon::now(),
                'created_at'        => Carbon::now()
            ],
            [
                'u_name'            => 'Sajed Mahmud',
                'u_mobile'          => '+8801722336699',
                'email'             => null,
                'email_verified_at' => null,
                'u_password'        => '$2y$10$JRPG8k5vb8D93p9hsLoDx.S5ftmo.jRYOoPUBVGF58Av/eTDMfS1u',
                'u_address'         => 'Notun bazar, Badda, Dhaka.',
                'u_image'           => 'storage/app/users/sajed.jpg',
                'ur_id'             => 3,
                'updated_at'        => Carbon::now(),
                'created_at'        => Carbon::now()
            ],
            [
                'u_name'            => 'Emon Khan',
                'u_mobile'          => '+8801788995522',
                'email'             => null,
                'email_verified_at' => null,
                'u_password'        => '$2y$10$1fSsS9blDopTUooB0H732OqtO7jeZFHDPbe6McZjqfid4cOH4bd.2',
                'u_address'         => 'Notun Bazar, Badda, Dhaka',
                'u_image'           => 'storage/app/users/emon.jpg',
                'ur_id'             => 2,
                'updated_at'        => Carbon::now(),
                'created_at'        => Carbon::now()
            ],
            [
                'u_name'            => 'Selim Rahman',
                'u_mobile'          => '+8801355201496',
                'email'             => null,
                'email_verified_at' => null,
                'u_password'        => '$2y$10$zAd001JYfon27ueUPB3q7OYcKcBzjcGE1fO4ZrgRvc9V8jRijR9xO',
                'u_address'         => 'Notun Bazar, Badda, Dhaka',
                'u_image'           => 'storage/app/users/selim.jpg',
                'ur_id'             => 4,
                'updated_at'        => Carbon::now(),
                'created_at'        => Carbon::now()
            ],
            [
                'u_name'            => 'Naimul Islam Naim',
                'u_mobile'          => '+8801752663311',
                'email'             => null,
                'email_verified_at' => null,
                'u_password'        => '$2y$10$6Uy5bp19i69L88yb2u9aSuaIJvcq5wWR6.lbDdY3rjBDlSyd3/PMq',
                'u_address'         => 'Mirpur 12, Dhaka',
                'u_image'           => 'storage/app/users/naim.jpg',
                'ur_id'             => 1,
                'updated_at'        => Carbon::now(),
                'created_at'        => Carbon::now()
            ],
        ]);
    }
}
