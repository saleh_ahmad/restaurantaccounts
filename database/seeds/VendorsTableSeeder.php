<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendors')->insert([
            [
                'v_name'        => 'Asha',
                'v_mobile'      => '+8801700995533',
                'v_address'     => 'Notun bazar, Dhaka',
                'v_description' => null,
                'v_totalBill'   => 4000.00,
                'v_totalPaid'   => 4000.00,
                'v_due'         => 0.00,
                'updated_at'    => Carbon::now(),
                'created_at'    => Carbon::now()
            ],
            [
                'v_name'        => 'Like Foods',
                'v_mobile'      => '+8801622335511',
                'v_address'     => 'Bashundhara R/A, Dhaka',
                'v_description' => null,
                'v_totalBill'   => 0.00,
                'v_totalPaid'   => 0.00,
                'v_due'         => 0.00,
                'updated_at'    => Carbon::now(),
                'created_at'    => Carbon::now()
            ]
        ]);
    }
}
