<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class FoodMenuCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('foodmenucategories')->insert([
            [
                'fmc_name'        => 'Indian',
                'fmc_description' => null,
                'updated_at'      => Carbon::now(),
                'created_at'      => Carbon::now()
            ],
            [
                'fmc_name'        => 'Fast Food',
                'fmc_description' => null,
                'updated_at'      => Carbon::now(),
                'created_at'      => Carbon::now()
            ],
            [
                'fmc_name'        => 'Set Menu',
                'fmc_description' => null,
                'updated_at'      => Carbon::now(),
                'created_at'      => Carbon::now()
            ],
            [
                'fmc_name'        => 'Quick Meal',
                'fmc_description' => null,
                'updated_at'      => Carbon::now(),
                'created_at'      => Carbon::now()
            ]
        ]);
    }
}
