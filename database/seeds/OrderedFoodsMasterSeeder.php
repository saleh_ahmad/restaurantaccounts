<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OrderedFoodsMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orderedfoodsmaster')->insert([
            [
                'oft_id'            => 1,
                's_id'              => 1,
                'ofm_totalPrice'    => 420.00,
                'ofm_discountPrice' => 20.00,
                'ofm_finalPrice'    => 400.00,
                'updated_at'        => Carbon::now(),
                'created_at'        => Carbon::now()
            ]
        ]);
    }
}
