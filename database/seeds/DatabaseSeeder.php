<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CashBoxSeeder::class,
            CashBoxTrnxCategoriesSeeder::class,
            CashBoxTrnxSeeder::class,
            FoodMenuCategoriesSeeder::class,
            FoodMenuImagesSeeder::class,
            FoodMenusSeeder::class,
            InventoryOrderDetailsSeeder::class,
            InventoryOrderMasterSeeder::class,
            InventorySeeder::class,
            OrderedFoodsDetailsSeeder::class,
            OrderedFoodsMasterSeeder::class,
            OrderedFoodsTypesSeeder::class,
            OwnersTableSeeder::class,
            RawMaterialsTableSeeder::class,
            SalaryTransactionTableSeeder::class,
            StaffTableSeeder::class,
            UserrolesTableSeeder::class,
            UsersTableSeeder::class,
            VendorsTableSeeder::class
        ]);
    }
}
