<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class StaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('staff')->insert([
            [
                'u_id'          => 3,
                's_salary'      => 20000.00,
                's_joiningDate' => '2019-03-01',
                's_endingDate'  => null,
                'updated_at'    => Carbon::now(),
                'created_at'    => Carbon::now()
            ],
            [
                'u_id'          => 2,
                's_salary'      => 15000.00,
                's_joiningDate' => '2019-03-01',
                's_endingDate'  => null,
                'updated_at'    => Carbon::now(),
                'created_at'    => Carbon::now()
            ],
            [
                'u_id'          => 4,
                's_salary'      => 5000.00,
                's_joiningDate' => '2019-03-01',
                's_endingDate'  => null,
                'updated_at'    => Carbon::now(),
                'created_at'    => Carbon::now()
            ]
        ]);
    }
}
