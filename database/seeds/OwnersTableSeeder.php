<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OwnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('owners')->insert([
            [
                'u_id'         => 1,
                'o_totalDue'   => 0.00,
                'o_totalGiven' => 80000.00,
                'updated_at'   => Carbon::now(),
                'created_at'   => Carbon::now()
            ],
            [
                'u_id'         => 5,
                'o_totalDue'   => 0.00,
                'o_totalGiven' => 0.00,
                'updated_at'   => Carbon::now(),
                'created_at'   => Carbon::now()
            ]
        ]);
    }
}
