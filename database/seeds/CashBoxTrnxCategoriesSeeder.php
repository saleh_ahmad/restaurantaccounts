<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CashBoxTrnxCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cashboxtrnxcategories')->insert([
            [
                'cbtc_name'        => 'Cash In',
                'cbtc_description' => 'Cash In From owners',
                'updated_at'       => Carbon::now(),
                'created_at'       => Carbon::now()
            ],
            [
                'cbtc_name'        => 'Cash Out',
                'cbtc_description' => 'Cash Out from owners',
                'updated_at'       => Carbon::now(),
                'created_at'       => Carbon::now()
            ],
            [
                'cbtc_name'        => 'Food Orders',
                'cbtc_description' => 'Food Orders By the Customers',
                'updated_at'       => Carbon::now(),
                'created_at'       => Carbon::now()
            ],
            [
                'cbtc_name'        => 'Other Costs',
                'cbtc_description' => 'Gas Bill, Soft Drinks etc.',
                'updated_at'       => Carbon::now(),
                'created_at'       => Carbon::now()
            ],
            [
                'cbtc_name'        => 'Vendor\'s Bill',
                'cbtc_description' => 'Vendor\'s Daily Bill',
                'updated_at'       => Carbon::now(),
                'created_at'       => Carbon::now()
            ],
            [
                'cbtc_name'        => 'Salary Paid',
                'cbtc_description' => 'Staff Salary',
                'updated_at'       => Carbon::now(),
                'created_at'       => Carbon::now()
            ],
            [
                'cbtc_name'        => 'Staff Food Bill',
                'cbtc_description' => 'Staff Food Bill',
                'updated_at'       => Carbon::now(),
                'created_at'       => Carbon::now()
            ]
        ]);
    }
}
