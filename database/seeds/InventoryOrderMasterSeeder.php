<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class InventoryOrderMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inventoryordermaster')->insert([
            [
                'v_id'               => 1,
                'iom_description'    => null,
                'iom_bill'           => 4000.00,
                'iom_billCopyImage'  => 'assets/images/bills/27.03.2019.jpg',
                'iom_paid'           => 4000.00,
                'iom_date'           => '2019-04-16',
                'iom_orderedUserId'  => 2,
                'iom_receivedUserId' => 3,
                'updated_at'         => Carbon::now(),
                'created_at'         => Carbon::now()
            ]
        ]);
    }
}
