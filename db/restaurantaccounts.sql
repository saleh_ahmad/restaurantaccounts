-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 16, 2019 at 04:14 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurantaccounts`
--

-- --------------------------------------------------------

--
-- Table structure for table `cashbox`
--

CREATE TABLE `cashbox` (
  `cb_id` int(11) NOT NULL COMMENT 'cash box id, primary key, auto increment starts from 1',
  `cb_amountInTheCashBox` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'amount found in the cash box',
  `cb_totalDebit` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'total earning of the day (cash-in + food order)',
  `cb_totalCredit` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'total costs of the day (vendor''s bill + other costs + cash out + salary paid)',
  `cb_expectedAmount` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'cb_totalDebit - cb_totalCredit',
  `cb_date` date NOT NULL COMMENT 'date of the day, not nullable'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='table holds the cash box condition information of everyday transaction';

--
-- Dumping data for table `cashbox`
--

INSERT INTO `cashbox` (`cb_id`, `cb_amountInTheCashBox`, `cb_totalDebit`, `cb_totalCredit`, `cb_expectedAmount`, `cb_date`) VALUES
(1, '400.00', '80400.00', '44000.00', '40400.00', '2019-03-28');

-- --------------------------------------------------------

--
-- Table structure for table `cashboxtrnx`
--

CREATE TABLE `cashboxtrnx` (
  `cbt_id` int(11) NOT NULL COMMENT 'cash box transaction id, primary key, auto increment starts from 1',
  `cbt_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'type of the transaction, 1 for debit, 2 for credit',
  `cbt_amount` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'cash box transaction amount',
  `cbt_purpose` text COMMENT 'purpose of the transaction',
  `cbtc_id` int(3) NOT NULL DEFAULT '0' COMMENT 'cash box transaction category id, foreign key, comes from cashBoxTrnxCategories.cbtc_id',
  `u_id` int(11) NOT NULL DEFAULT '0' COMMENT 'user id who associated with the transaction, foreign key, comes from users.u_id',
  `cbt_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Transaction time and date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table holds the all types of cash box transaction information';

--
-- Dumping data for table `cashboxtrnx`
--

INSERT INTO `cashboxtrnx` (`cbt_id`, `cbt_type`, `cbt_amount`, `cbt_purpose`, `cbtc_id`, `u_id`, `cbt_datetime`) VALUES
(1, 1, '400.00', 'Customer Order', 3, 3, '2019-03-28 13:51:25'),
(2, 1, '80000.00', 'Investment', 1, 1, '2019-03-28 13:51:25'),
(3, 2, '20000.00', 'Give salary to the Manager', 6, 1, '2019-03-28 14:06:34'),
(4, 2, '15000.00', 'Give salary to the Chef', 6, 1, '2019-03-28 14:15:36'),
(5, 2, '5000.00', 'Give salary to the Cleaner', 6, 1, '2019-03-28 14:16:12'),
(6, 2, '4000.00', 'Bill Paid (Asha)', 5, 3, '2019-03-28 16:07:55');

-- --------------------------------------------------------

--
-- Table structure for table `cashboxtrnxcategories`
--

CREATE TABLE `cashboxtrnxcategories` (
  `cbtc_id` int(3) NOT NULL COMMENT 'cash box transaction categories id, primary key, auto increment starts from 1',
  `cbtc_name` varchar(128) CHARACTER SET latin1 NOT NULL COMMENT 'cash box transaction category name i.e. cash-in, food order',
  `cbtc_description` text CHARACTER SET latin1 COMMENT 'cash box transaction category description'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table holds all types of cash box transactio histories i.e cash-in, cash-out, foodorder, othercosts, vendor''s bill, salary paid, staff Food Bill';

--
-- Dumping data for table `cashboxtrnxcategories`
--

INSERT INTO `cashboxtrnxcategories` (`cbtc_id`, `cbtc_name`, `cbtc_description`) VALUES
(1, 'Cash In', 'Cash In From owners'),
(2, 'Cash Out', 'Cash Out from owners'),
(3, 'Food Orders', 'Food Orders By the Customers'),
(4, 'Other Costs', 'Gas Bill, Soft Drinks etc.'),
(5, 'Vendor\'s Bill', 'Vendor\'s Daily Bill'),
(6, 'Salary Paid', 'Staff Salary'),
(7, 'Staff Food Bill', 'Staff Food Bill');

-- --------------------------------------------------------

--
-- Table structure for table `foodmenucategories`
--

CREATE TABLE `foodmenucategories` (
  `fmc_id` int(11) NOT NULL COMMENT 'food menu category id, primary key, auto increment starts from 1',
  `fmc_name` varchar(100) CHARACTER SET latin1 NOT NULL COMMENT 'food menu category name, not nullable',
  `fmc_description` text CHARACTER SET latin1 COMMENT 'food menu category description, nullable'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table holds different types of food menu categories i.e. indian, fast food, set menu, quick meal';

--
-- Dumping data for table `foodmenucategories`
--

INSERT INTO `foodmenucategories` (`fmc_id`, `fmc_name`, `fmc_description`) VALUES
(1, 'Indian', NULL),
(2, 'Fast Food', NULL),
(3, 'Set Menu', NULL),
(4, 'Quick Meal', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `foodmenuimages`
--

CREATE TABLE `foodmenuimages` (
  `fmi_id` int(11) NOT NULL COMMENT 'food menu image id, primary key, auto increment starts from 1',
  `fm_id` int(11) NOT NULL DEFAULT '0' COMMENT 'food menu id, foreign key, comes from foodMenus.fm_id',
  `fmi_image` text NOT NULL COMMENT 'food menu image path, not nullable',
  `fmi_insertedDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'food menu image inserted timestamp (current timestamp)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table holds one or more than one of every food menu image';

--
-- Dumping data for table `foodmenuimages`
--

INSERT INTO `foodmenuimages` (`fmi_id`, `fm_id`, `fmi_image`, `fmi_insertedDateTime`) VALUES
(1, 1, 'assets/images/foodmenu/studentMenu01.jpg', '2019-03-28 12:27:55'),
(2, 3, 'assets/images/foodmenu/studentMenu02.jpg', '2019-03-28 12:27:55');

-- --------------------------------------------------------

--
-- Table structure for table `foodmenus`
--

CREATE TABLE `foodmenus` (
  `fm_id` int(11) NOT NULL COMMENT 'food menu id, primary key, auto increment starts from 1',
  `fm_name` varchar(128) CHARACTER SET latin1 NOT NULL COMMENT 'food menu name, not nullable',
  `fm_description` text CHARACTER SET latin1 COMMENT 'food menu description, null when no description added',
  `fmc_id` int(11) NOT NULL DEFAULT '0' COMMENT 'food menu category id, foreign key, comes from foodMenuCategories.fmc_id',
  `fm_makingCost` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT 'making cost of the food menu',
  `fm_sellingPrice` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT 'selling cost of the food menu',
  `fm_profit` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT 'profit over the food menu (fm_sellingPrice - fm_makingCost)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table holds the food menu information';

--
-- Dumping data for table `foodmenus`
--

INSERT INTO `foodmenus` (`fm_id`, `fm_name`, `fm_description`, `fmc_id`, `fm_makingCost`, `fm_sellingPrice`, `fm_profit`) VALUES
(1, 'Student Meal 01', 'Fried Rice, Chicken Fry, Gravy vegetables', 3, '80.00', '140.00', '60.00'),
(3, 'Student Meal 02', 'Fried Rice, Spicy Chicken Wings (2 pc), Gravy Vegetables', 3, '120.00', '170.00', '50.00');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `inv_id` bigint(20) NOT NULL COMMENT 'inventory id, primary key, auto increment starts from 1',
  `rm_id` int(5) NOT NULL DEFAULT '0' COMMENT 'raw material id, foreign key, comes from  rawMaterials.rm_id',
  `Inv_beginningQty` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'beginning qty of the raw material, nullable',
  `Inv_orderedQty` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'ordered qty of the raw material, nullable',
  `Inv_rerceivedQty` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'received qty of the raw material, nullable',
  `inv_presentQty` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'present qty of the raw material (Inv_beginningQty + Inv_rerceivedQty), nullable',
  `inv_endingQty` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'ending qty of the raw material (beginning qty for the next day), nullable',
  `inv_usage` varchar(128) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'total usage qty of the day (inv_presentQty - inv_endingQty), nullable',
  `inv_date` date NOT NULL COMMENT 'date of the day'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table holds the inventory  information';

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`inv_id`, `rm_id`, `Inv_beginningQty`, `Inv_orderedQty`, `Inv_rerceivedQty`, `inv_presentQty`, `inv_endingQty`, `inv_usage`, `inv_date`) VALUES
(1, 1, '0kg', '10kg', '10kg', '10kg', '3kg', '7kg', '2019-03-28');

-- --------------------------------------------------------

--
-- Table structure for table `inventoryorderdetails`
--

CREATE TABLE `inventoryorderdetails` (
  `iod_id` bigint(20) NOT NULL COMMENT 'inventory order details id, primary key, auto increment id starts from 1',
  `rm_id` int(5) NOT NULL DEFAULT '0' COMMENT 'raw materials id, foreign id, comes from rawMaterials.rm_id',
  `iod_unitPrice` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'inventory ordered raw material unit price, it may vary',
  `iod_orderedQuantity` varchar(128) DEFAULT NULL COMMENT 'quantity of the ordered raw material',
  `iod_receivedQuantity` varchar(128) DEFAULT NULL COMMENT 'quantity of the received raw material',
  `iod_totalPrice` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'total price of the raw material (iod_unitPrice * iod_quantity)',
  `iom_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'inventory order master, foreign key, comes from inventoryOrderMaster.iom_id',
  `iod_receivedRawMaterialImage` text COMMENT 'image of the ordered raw material'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table holds the raw material order details information (child table of the inventoryOrderMaster)';

--
-- Dumping data for table `inventoryorderdetails`
--

INSERT INTO `inventoryorderdetails` (`iod_id`, `rm_id`, `iod_unitPrice`, `iod_orderedQuantity`, `iod_receivedQuantity`, `iod_totalPrice`, `iom_id`, `iod_receivedRawMaterialImage`) VALUES
(1, 1, '400.00', '10kg', '10kg', '4000.00', 1, 'assets/images/receivedItems/27.03.2019.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `inventoryordermaster`
--

CREATE TABLE `inventoryordermaster` (
  `iom_id` bigint(20) NOT NULL COMMENT 'inventory order master id, primary key, auto increment starts from 1',
  `v_id` int(11) NOT NULL DEFAULT '0' COMMENT 'vendor id, foreign key, comes from vendors.v_id',
  `iom_description` text COMMENT 'description of the order',
  `iom_bill` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'Total Bill of the order',
  `iom_billCopyImage` text COMMENT 'Bill copy image of the order',
  `iom_paid` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'total paid of the bill',
  `iom_date` date NOT NULL COMMENT 'date of the order',
  `iom_orderedUserId` int(11) NOT NULL DEFAULT '0' COMMENT 'user who order the raw materials',
  `iom_receivedUserId` int(11) NOT NULL DEFAULT '0' COMMENT 'user who receive the raw materials'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table holds the inventory raw materials order information (Parent Table of the inventoryOrderDetails)';

--
-- Dumping data for table `inventoryordermaster`
--

INSERT INTO `inventoryordermaster` (`iom_id`, `v_id`, `iom_description`, `iom_bill`, `iom_billCopyImage`, `iom_paid`, `iom_date`, `iom_orderedUserId`, `iom_receivedUserId`) VALUES
(1, 1, NULL, '4000.00', 'assets/images/bills/27.03.2019.jpg', '4000.00', '2019-03-28', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `orderedfoodsdetails`
--

CREATE TABLE `orderedfoodsdetails` (
  `ofd_id` bigint(20) NOT NULL COMMENT 'ordered food details id, primary key, auto increment starts from 1',
  `ofm_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'ofdered food master id, foreign key, comes from orderedFoodsMaster.ofm_id',
  `fm_id` int(11) NOT NULL DEFAULT '0' COMMENT 'food menu id, foreign key, comes from foodMenus.fm_id',
  `ofd_qty` int(5) NOT NULL DEFAULT '0' COMMENT 'order food menu quantity',
  `ofd_price` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'total price of the ordered food menu (unit price of the food menu * ofd_qty)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table holds the order details information of every order (child table of orderedFoodsMaster table)';

--
-- Dumping data for table `orderedfoodsdetails`
--

INSERT INTO `orderedfoodsdetails` (`ofd_id`, `ofm_id`, `fm_id`, `ofd_qty`, `ofd_price`) VALUES
(1, 1, 1, 3, '420.00');

-- --------------------------------------------------------

--
-- Table structure for table `orderedfoodsmaster`
--

CREATE TABLE `orderedfoodsmaster` (
  `ofm_id` bigint(20) NOT NULL COMMENT 'ordered food master id, primary key, suto increment starts from 1',
  `oft_id` int(3) NOT NULL DEFAULT '0' COMMENT 'ordered food type id, foreign key, comes from orderedFoodsTypes.oft_id',
  `s_id` int(11) NOT NULL DEFAULT '0' COMMENT 'stuff id who take the order, foreign key, comes from staff.s_id',
  `ofm_totalPrice` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'total price of the ordered foods',
  `ofm_discountPrice` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'discount price of the ordered foods',
  `ofm_finalPrice` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'final price of the ordered foods (totalPrice - DiscountPrice)',
  `ofm_dateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'timestamp of the ordered foods'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table holds the ordered food information (Parent table of orderedFoodsDetails table)';

--
-- Dumping data for table `orderedfoodsmaster`
--

INSERT INTO `orderedfoodsmaster` (`ofm_id`, `oft_id`, `s_id`, `ofm_totalPrice`, `ofm_discountPrice`, `ofm_finalPrice`, `ofm_dateTime`) VALUES
(1, 1, 1, '420.00', '20.00', '400.00', '2019-03-28 12:24:40');

-- --------------------------------------------------------

--
-- Table structure for table `orderedfoodstypes`
--

CREATE TABLE `orderedfoodstypes` (
  `oft_id` int(3) NOT NULL COMMENT 'ordered food type id, primary key, auto incerement starts from 1',
  `oft_name` varchar(50) CHARACTER SET latin1 NOT NULL COMMENT 'order food type name i.e. shop, food panda, pathao',
  `oft_description` text CHARACTER SET latin1 COMMENT 'short or long long description of the type'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table holds the all types of food delivery types i.e. shop, food panda, hungrinaki, pathao ';

--
-- Dumping data for table `orderedfoodstypes`
--

INSERT INTO `orderedfoodstypes` (`oft_id`, `oft_name`, `oft_description`) VALUES
(1, 'Shop', NULL),
(2, 'Food panda', NULL),
(3, 'Hungry Naki', NULL),
(4, 'Pathao', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `owners`
--

CREATE TABLE `owners` (
  `o_id` int(11) NOT NULL COMMENT 'owner id, primary key, auto increent starts from 1',
  `u_id` int(11) NOT NULL DEFAULT '0' COMMENT 'user id, foreign key, comes from users.u_id',
  `o_totalDue` decimal(12,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'total due payment of the owner, can''t be negative',
  `o_totalGiven` decimal(12,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'total given money of the owner, can''t be negative'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table holds the owners due and given money information';

--
-- Dumping data for table `owners`
--

INSERT INTO `owners` (`o_id`, `u_id`, `o_totalDue`, `o_totalGiven`) VALUES
(1, 1, '0.00', '80000.00'),
(2, 5, '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `rawmaterials`
--

CREATE TABLE `rawmaterials` (
  `rm_id` int(5) NOT NULL COMMENT 'raw materials id, primary key, auto increment starts from 1',
  `rm_name` varchar(128) NOT NULL COMMENT 'raw material name i.e. oil, sugar, cheese, bun',
  `rm_description` text COMMENT 'description of the raw material',
  `rm_image` text COMMENT 'raw material''s image'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table holds the raw materials of the food menu';

--
-- Dumping data for table `rawmaterials`
--

INSERT INTO `rawmaterials` (`rm_id`, `rm_name`, `rm_description`, `rm_image`) VALUES
(1, 'Boneless Chicken', NULL, 'assets/images/rawmaterials/01.jpg'),
(2, 'Chicken Leg', NULL, 'assets/images/rawmaterials/02.jpg'),
(3, 'Chicken Wings', NULL, 'assets/images/rawmaterials/03.jpg'),
(4, 'Whole Chicken', NULL, 'assets/images/rawmaterials/04.jpg'),
(5, 'Alu (Diamond)', NULL, 'assets/images/rawmaterials/05.jpg'),
(6, 'Piyaj (Indian)', NULL, 'assets/images/rawmaterials/06.jpg'),
(7, 'Roshun', NULL, 'assets/images/rawmaterials/07.jpg'),
(8, 'Ada', NULL, 'assets/images/rawmaterials/08.jpg'),
(9, 'Thai Ada', NULL, 'assets/images/rawmaterials/09.jpg'),
(10, 'Moyda', NULL, 'assets/images/rawmaterials/10.jpg'),
(11, 'Polao Rice', NULL, 'assets/images/rawmaterials/11.jpg'),
(12, 'Soyabean Oil', NULL, 'assets/images/rawmaterials/12.jpg'),
(13, 'Salt', NULL, 'assets/images/rawmaterials/13.jpg'),
(14, 'Sugar', NULL, 'assets/images/rawmaterials/14.jpg'),
(15, 'Thai Oyster Sauce', NULL, 'assets/images/rawmaterials/15.jpg'),
(16, 'Dragon Noodles', NULL, 'assets/images/rawmaterials/16.jpg'),
(17, 'Wheep Cream', NULL, 'assets/images/rawmaterials/17.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `salarytransaction`
--

CREATE TABLE `salarytransaction` (
  `st_id` bigint(20) NOT NULL COMMENT 'salary transaction id, primary key, Auto increment starts from 1',
  `st_givenSalary` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'given salary to the staff by a owner',
  `st_totalPaidSalary` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'total paid salary in that month to the staff by a owners',
  `st_totalDueSalary` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'total due salary in that month to the staff by the owners',
  `s_id` int(11) NOT NULL DEFAULT '0' COMMENT 'staff id, foreign key, comes from staff.s_id',
  `o_id` int(11) NOT NULL DEFAULT '0' COMMENT 'owner id, foreign key, comes from owners.o_id',
  `st_month` date NOT NULL COMMENT 'month of the given salary to the staff by a owner, not nullable',
  `st_givenDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'timestamp of the salary transaction'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table holds the staff salary transaction records.';

--
-- Dumping data for table `salarytransaction`
--

INSERT INTO `salarytransaction` (`st_id`, `st_givenSalary`, `st_totalPaidSalary`, `st_totalDueSalary`, `s_id`, `o_id`, `st_month`, `st_givenDate`) VALUES
(1, '20000.00', '20000.00', '0.00', 1, 1, '2019-02-01', '2019-03-28 12:08:10'),
(2, '15000.00', '15000.00', '0.00', 2, 1, '2019-02-01', '2019-03-28 12:08:10'),
(3, '5000.00', '5000.00', '0.00', 3, 1, '2019-02-01', '2019-03-28 12:08:30');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `s_id` int(11) NOT NULL COMMENT 'staff id, primary key, auto increment starts from 1',
  `u_id` int(11) NOT NULL DEFAULT '0' COMMENT 'user id, foreign key, comes from users.u_id',
  `s_salary` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'staff salary from the owners, can''t be negative',
  `s_joiningDate` date DEFAULT NULL COMMENT 'staff joining date, null if no joining date will input',
  `s_endingDate` date DEFAULT NULL COMMENT 'staff resigning date, null if staff is currently working'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table holds only the information related with the staff';

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`s_id`, `u_id`, `s_salary`, `s_joiningDate`, `s_endingDate`) VALUES
(1, 3, '20000.00', '2019-02-14', NULL),
(2, 2, '15000.00', '2019-02-14', NULL),
(3, 4, '5000.00', '2019-02-14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userroles`
--

CREATE TABLE `userroles` (
  `ur_id` int(3) NOT NULL COMMENT 'user role auto generated id, primary key, auto increment starts from 1',
  `ur_name` varchar(20) NOT NULL COMMENT 'user role name i.e. owner, staff, not nullable',
  `ur_description` text COMMENT 'user role description, null if there is no description for the user role'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table holds the different tyes of user roles information.';

--
-- Dumping data for table `userroles`
--

INSERT INTO `userroles` (`ur_id`, `ur_name`, `ur_description`) VALUES
(1, 'Owner', 'Owner of the restaurant.'),
(2, 'Manager', 'Restaurant manager'),
(3, 'Chef', 'Restaurant chef.'),
(4, 'Cleaner', 'Restaurant Cleaner.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `u_id` int(11) NOT NULL COMMENT 'user auto generated id, primary key, auto incremented column starting from 1.',
  `u_name` varchar(128) NOT NULL COMMENT 'user name, not nullable',
  `u_mobile` varchar(20) NOT NULL COMMENT 'user mobile number, not nullable, must be unique as it will use as an unique identifier in the application.',
  `u_password` varchar(256) NOT NULL COMMENT 'user password, not nullable, encrypted from the application.',
  `u_address` text COMMENT 'user address, null if user won''t give any address',
  `u_image` text COMMENT 'user profile image. name can be too big, null if user won''t give any image of him/her. ',
  `ur_id` int(3) NOT NULL DEFAULT '0' COMMENT 'user role id, foreign key, comes from userRoles.ur_id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table holds the owners and the stuff common information';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`u_id`, `u_name`, `u_mobile`, `u_password`, `u_address`, `u_image`, `ur_id`) VALUES
(1, 'Hasan Mahmud Maruf', '+8801670445591', '$2y$10$t8YCD9EbQIn8zoCboLpCrOLCkvS0/YGFKKa/RUoWGx43U8//eT5zi', '08/161, 12/E, Mirpur, Dhaka - 1216', 'images/users/maruf.jpg', 1),
(2, 'Sajed mahmud', '+8801722336699', '$2y$10$JRPG8k5vb8D93p9hsLoDx.S5ftmo.jRYOoPUBVGF58Av/eTDMfS1u', 'Notun bazar, Badda, Dhaka.', 'images/users/sajed.jpg', 3),
(3, 'Emon Khan', '+8801788995522', '$2y$10$1fSsS9blDopTUooB0H732OqtO7jeZFHDPbe6McZjqfid4cOH4bd.2', 'Notun Bazar, Badda, Dhaka', 'images/users/emon.jpg', 2),
(4, 'Selim Rahman', '+8801355201496', '$2y$10$zAd001JYfon27ueUPB3q7OYcKcBzjcGE1fO4ZrgRvc9V8jRijR9xO', 'Notun Bazar, Badda, Dhaka', 'images/users/selim.jpg', 4),
(5, 'Naimul Islam Naim', '+8801752663311', '$2y$10$6Uy5bp19i69L88yb2u9aSuaIJvcq5wWR6.lbDdY3rjBDlSyd3/PMq', 'Mirpur 12, Dhaka', 'images/users/naim.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `v_id` int(11) NOT NULL COMMENT 'vendor id, primary key, auto increment starts from 1',
  `v_name` varchar(128) CHARACTER SET latin1 NOT NULL COMMENT 'vendor name i.e. Asha, Like foods',
  `v_mobile` varchar(20) CHARACTER SET latin1 NOT NULL COMMENT 'vendor mobile number, + is also allowed, must be unique',
  `v_address` text CHARACTER SET latin1 COMMENT 'vendor''s shop address, nullable',
  `v_description` text CHARACTER SET latin1 COMMENT 'short description about a vendor, nullable',
  `v_totalBill` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'vendor''s lifetime bill',
  `V_totalPaid` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'total paid amount to the vendors',
  `v_due` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'vendor''s total due amount'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Table holds the the vendors information who supplies the raw materials';

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`v_id`, `v_name`, `v_mobile`, `v_address`, `v_description`, `v_totalBill`, `V_totalPaid`, `v_due`) VALUES
(1, 'Asha', '+8801700995533', 'Notun bazar, Dhaka', NULL, '4000.00', '4000.00', '0.00'),
(2, 'Like Foods', '+8801622335511', 'Bashundhara R/A, Dhaka', NULL, '0.00', '0.00', '0.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cashbox`
--
ALTER TABLE `cashbox`
  ADD PRIMARY KEY (`cb_id`);

--
-- Indexes for table `cashboxtrnx`
--
ALTER TABLE `cashboxtrnx`
  ADD PRIMARY KEY (`cbt_id`),
  ADD KEY `FK1_cbtc_id` (`cbtc_id`),
  ADD KEY `FK2_u_id` (`u_id`);

--
-- Indexes for table `cashboxtrnxcategories`
--
ALTER TABLE `cashboxtrnxcategories`
  ADD PRIMARY KEY (`cbtc_id`);

--
-- Indexes for table `foodmenucategories`
--
ALTER TABLE `foodmenucategories`
  ADD PRIMARY KEY (`fmc_id`);

--
-- Indexes for table `foodmenuimages`
--
ALTER TABLE `foodmenuimages`
  ADD PRIMARY KEY (`fmi_id`),
  ADD KEY `FK1_fm_id` (`fm_id`);

--
-- Indexes for table `foodmenus`
--
ALTER TABLE `foodmenus`
  ADD PRIMARY KEY (`fm_id`),
  ADD KEY `FK1_fmc_id` (`fmc_id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`inv_id`),
  ADD KEY `FK_rm_id` (`rm_id`);

--
-- Indexes for table `inventoryorderdetails`
--
ALTER TABLE `inventoryorderdetails`
  ADD PRIMARY KEY (`iod_id`),
  ADD KEY `FK1_rm_id` (`rm_id`),
  ADD KEY `FK2_iom_id` (`iom_id`);

--
-- Indexes for table `inventoryordermaster`
--
ALTER TABLE `inventoryordermaster`
  ADD PRIMARY KEY (`iom_id`),
  ADD KEY `FK1_v_id` (`v_id`),
  ADD KEY `FK2_iom_orderedUserId` (`iom_orderedUserId`),
  ADD KEY `FK3_iom_receivedUserId` (`iom_receivedUserId`);

--
-- Indexes for table `orderedfoodsdetails`
--
ALTER TABLE `orderedfoodsdetails`
  ADD PRIMARY KEY (`ofd_id`),
  ADD KEY `FK1_ofm_id` (`ofm_id`),
  ADD KEY `FK2_fm_id` (`fm_id`);

--
-- Indexes for table `orderedfoodsmaster`
--
ALTER TABLE `orderedfoodsmaster`
  ADD PRIMARY KEY (`ofm_id`),
  ADD KEY `FK1_oft_id` (`oft_id`),
  ADD KEY `FK2_s_id` (`s_id`);

--
-- Indexes for table `orderedfoodstypes`
--
ALTER TABLE `orderedfoodstypes`
  ADD PRIMARY KEY (`oft_id`);

--
-- Indexes for table `owners`
--
ALTER TABLE `owners`
  ADD PRIMARY KEY (`o_id`),
  ADD KEY `FK_u_id` (`u_id`);

--
-- Indexes for table `rawmaterials`
--
ALTER TABLE `rawmaterials`
  ADD PRIMARY KEY (`rm_id`);

--
-- Indexes for table `salarytransaction`
--
ALTER TABLE `salarytransaction`
  ADD PRIMARY KEY (`st_id`),
  ADD KEY `FK1_s_id` (`s_id`),
  ADD KEY `FK2_o_id` (`o_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `userroles`
--
ALTER TABLE `userroles`
  ADD PRIMARY KEY (`ur_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`u_id`),
  ADD UNIQUE KEY `u_mobile` (`u_mobile`),
  ADD KEY `FK_ur_id` (`ur_id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`v_id`),
  ADD UNIQUE KEY `v_mobile` (`v_mobile`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cashbox`
--
ALTER TABLE `cashbox`
  MODIFY `cb_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'cash box id, primary key, auto increment starts from 1', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cashboxtrnx`
--
ALTER TABLE `cashboxtrnx`
  MODIFY `cbt_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'cash box transaction id, primary key, auto increment starts from 1', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cashboxtrnxcategories`
--
ALTER TABLE `cashboxtrnxcategories`
  MODIFY `cbtc_id` int(3) NOT NULL AUTO_INCREMENT COMMENT 'cash box transaction categories id, primary key, auto increment starts from 1', AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `foodmenucategories`
--
ALTER TABLE `foodmenucategories`
  MODIFY `fmc_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'food menu category id, primary key, auto increment starts from 1', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `foodmenuimages`
--
ALTER TABLE `foodmenuimages`
  MODIFY `fmi_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'food menu image id, primary key, auto increment starts from 1', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `foodmenus`
--
ALTER TABLE `foodmenus`
  MODIFY `fm_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'food menu id, primary key, auto increment starts from 1', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `inv_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'inventory id, primary key, auto increment starts from 1', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `inventoryorderdetails`
--
ALTER TABLE `inventoryorderdetails`
  MODIFY `iod_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'inventory order details id, primary key, auto increment id starts from 1', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `inventoryordermaster`
--
ALTER TABLE `inventoryordermaster`
  MODIFY `iom_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'inventory order master id, primary key, auto increment starts from 1', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orderedfoodsdetails`
--
ALTER TABLE `orderedfoodsdetails`
  MODIFY `ofd_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ordered food details id, primary key, auto increment starts from 1', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orderedfoodsmaster`
--
ALTER TABLE `orderedfoodsmaster`
  MODIFY `ofm_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ordered food master id, primary key, suto increment starts from 1', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orderedfoodstypes`
--
ALTER TABLE `orderedfoodstypes`
  MODIFY `oft_id` int(3) NOT NULL AUTO_INCREMENT COMMENT 'ordered food type id, primary key, auto incerement starts from 1', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `owners`
--
ALTER TABLE `owners`
  MODIFY `o_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'owner id, primary key, auto increent starts from 1', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rawmaterials`
--
ALTER TABLE `rawmaterials`
  MODIFY `rm_id` int(5) NOT NULL AUTO_INCREMENT COMMENT 'raw materials id, primary key, auto increment starts from 1', AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `salarytransaction`
--
ALTER TABLE `salarytransaction`
  MODIFY `st_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'salary transaction id, primary key, Auto increment starts from 1', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'staff id, primary key, auto increment starts from 1', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `userroles`
--
ALTER TABLE `userroles`
  MODIFY `ur_id` int(3) NOT NULL AUTO_INCREMENT COMMENT 'user role auto generated id, primary key, auto increment starts from 1', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'user auto generated id, primary key, auto incremented column starting from 1.', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `v_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'vendor id, primary key, auto increment starts from 1', AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cashboxtrnx`
--
ALTER TABLE `cashboxtrnx`
  ADD CONSTRAINT `FK1_cbtc_id` FOREIGN KEY (`cbtc_id`) REFERENCES `cashboxtrnxcategories` (`cbtc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_u_id` FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `foodmenuimages`
--
ALTER TABLE `foodmenuimages`
  ADD CONSTRAINT `FK1_fm_id` FOREIGN KEY (`fm_id`) REFERENCES `foodmenus` (`fm_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `foodmenus`
--
ALTER TABLE `foodmenus`
  ADD CONSTRAINT `FK1_fmc_id` FOREIGN KEY (`fmc_id`) REFERENCES `foodmenucategories` (`fmc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `FK_rm_id` FOREIGN KEY (`rm_id`) REFERENCES `rawmaterials` (`rm_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `inventoryorderdetails`
--
ALTER TABLE `inventoryorderdetails`
  ADD CONSTRAINT `FK1_rm_id` FOREIGN KEY (`rm_id`) REFERENCES `rawmaterials` (`rm_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_iom_id` FOREIGN KEY (`iom_id`) REFERENCES `inventoryordermaster` (`iom_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `inventoryordermaster`
--
ALTER TABLE `inventoryordermaster`
  ADD CONSTRAINT `FK1_v_id` FOREIGN KEY (`v_id`) REFERENCES `vendors` (`v_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_iom_orderedUserId` FOREIGN KEY (`iom_orderedUserId`) REFERENCES `users` (`u_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_iom_receivedUserId` FOREIGN KEY (`iom_receivedUserId`) REFERENCES `users` (`u_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `orderedfoodsdetails`
--
ALTER TABLE `orderedfoodsdetails`
  ADD CONSTRAINT `FK1_ofm_id` FOREIGN KEY (`ofm_id`) REFERENCES `orderedfoodsmaster` (`ofm_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_fm_id` FOREIGN KEY (`fm_id`) REFERENCES `foodmenus` (`fm_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `orderedfoodsmaster`
--
ALTER TABLE `orderedfoodsmaster`
  ADD CONSTRAINT `FK1_oft_id` FOREIGN KEY (`oft_id`) REFERENCES `orderedfoodstypes` (`oft_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_s_id` FOREIGN KEY (`s_id`) REFERENCES `staff` (`s_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `owners`
--
ALTER TABLE `owners`
  ADD CONSTRAINT `FK_u_id` FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `salarytransaction`
--
ALTER TABLE `salarytransaction`
  ADD CONSTRAINT `FK1_s_id` FOREIGN KEY (`s_id`) REFERENCES `staff` (`s_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_o_id` FOREIGN KEY (`o_id`) REFERENCES `owners` (`o_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_ur_id` FOREIGN KEY (`ur_id`) REFERENCES `userroles` (`ur_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
