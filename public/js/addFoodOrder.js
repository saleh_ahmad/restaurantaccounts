$('#addFood').click(function(){
    var foodId = $('#food').val(),
        qty = $('#quantity').val();

    if (foodId === '') {
        $('#add-food-alert').html('<div class="alert alert-danger">Please Select a Food Menu</div>');
        return;
    }
    if (qty === '') {
        $('#add-food-alert').html('<div class="alert alert-danger">Please Enter Quantity</div>');
        return;
    }

    $.ajax({
        url: base +'/foodprice',
        type: 'GET',
        dataType: 'json',
        data: {
            id : foodId
        },
        error: function(response) {

        },
        success: function(response) {
            var price = parseInt(qty) * parseFloat(response.food.price);

            var html = '<tr>' +
                '<td>'+ response.food.fm_name +'<input type="hidden" name="food[]" value="'+ response.food.fm_id +'" /></td>' +
                '<td class="text-right">'+ parseFloat(response.food.price).toFixed(2) +'</td>' +
                '<td class="text-center">'+ qty +'<input type="hidden" name="qty[]" value="'+ qty +'" /></td>' +
                '<td class="text-right total-price-food">'+ Math.abs(price).toFixed(2) +'</td>' +
                '<td class="text-center">' +
                '<button type="button" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill delete-food" title="Delete" data-toggle="tooltip">' +
                '<i class="la la-trash"></i>' +
                '</button>' +
                '</td>' +
                '</tr>';

            $('#ordered-foods').append(html);

            var totalPrice = parseFloat($('#totalPrice').val());
            totalPrice += price;

            $('#totalPrice').val(Math.abs(totalPrice).toFixed(2));
            $('#finalPrice').val(Math.abs(totalPrice).toFixed(2));

            $('#food').val('');
            $('#quantity').val('1')
        }
    });
});

$('table#food-orders-table').on( 'click', '.delete-food', function() {
    var price = parseFloat($(this).parent().parent().find('td.total-price-food').html());

    var totalPrice = parseFloat($('#totalPrice').val());
    totalPrice -= price;

    $('#totalPrice').val(Math.abs(totalPrice).toFixed(2));
    $('#finalPrice').val(Math.abs(totalPrice).toFixed(2));

    $(this).parent().parent().remove();
});

$('#discount').blur(function(){
    var discount = parseFloat($(this).val()),
        finalPrice = parseFloat($('#finalPrice').val());

    finalPrice -= discount;

    $('#finalPrice').val(Math.abs(finalPrice).toFixed(2));
});
