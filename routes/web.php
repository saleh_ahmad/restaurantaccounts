<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//------------------------------------- Overriding Auth Routes ---------------------------------------------------------
Auth::routes();

// Authentication Routes...
Route::get('login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
]);
Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

// Password Reset Routes...
Route::post('password/email', [
    'as' => 'password.email',
    'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);
Route::get('password/reset', [
    'as' => 'password.request',
    'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);
Route::post('password/reset', [
    'as' => '',
    'uses' => 'Auth\ResetPasswordController@reset'
]);
Route::get('password/reset/{token}', [
    'as' => 'password.reset',
    'uses' => 'Auth\ResetPasswordController@showResetForm'
]);

// Registration Routes...
Route::get('signupvugichugiblabla741852963', [
    'as' => 'signupvugichugiblabla654321',
    'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('signupvugichugiblabla654321', [
    'as' => '',
    'uses' => 'Auth\RegisterController@register'
]);

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/', 'Auth\LoginController@showLoginForm');

Route::group(['middleware' => ['auth:web']], function() {
    Route::get('dashboard', 'DashboardController@index');
    Route::get('foodprice', 'FoodOrdersController@fetchFoodPrice');

    Route::resources([
        'staff'                   => 'StaffController',
        'salary-transaction'      => 'SalaryTransactionsController',
        'inventory'               => 'InventoryController',
        'inventory-orders'        => 'InventoryOrderController',
        'raw-materials'           => 'RawMaterialsController',
        'vendors'                 => 'VendorsController',
        'food-order-types'        => 'FoodOrderTypesController',
        'food-orders'             => 'FoodOrdersController',
        'food-menu-categories'    => 'FoodMenuCategoriesController',
        'food-menu'               => 'FoodMenuController',
        'cashbox'                 => 'CashBoxController',
        'cashbox-trnx'            => 'CashBoxTrnxController',
        'cashbox-trnx-categories' => 'CashBoxTrnxCategriesController'
    ]);
});
