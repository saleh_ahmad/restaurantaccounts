@extends('layouts.master')

@section('title', 'Cash Box Status')

@section('customcss')
    <link href="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('main')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{$status->cb_date}}
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="inventoryStatus">
                    <thead>
                    <tr>
                        <th class="text-center">Amount In the Cash Box</th>
                        <th class="text-center">Debit</th>
                        <th class="text-center">Credit</th>
                        <th class="text-center">Expected Amount</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-right">&#2547;  {{$status->cb_amountInTheCashBox}}</td>
                            <td class="text-right">&#2547;  {{$status->cb_totalDebit}}</td>
                            <td class="text-right">&#2547;  {{$status->cb_totalCredit}}</td>
                            <td class="text-right">&#2547;  {{number_format($status->cb_expectedAmount, 2, '.', '')}}</td>
                            <td class="text-center">
                                <a href="{{url('cashbox/'. $status->cb_id . '/edit')}}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">
                                    <i class="la la-edit"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('customjs')
    <script src="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/js/staff.js')}}" type="text/javascript"></script>
@endsection