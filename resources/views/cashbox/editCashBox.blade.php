@extends('layouts.master')

@section('title', 'Cash Box Amount')

@section('customcss')
    <link href="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('main')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">Cash Box Amount</h3>
                </div>
            </div>
        </div>

        <div class="m-portlet m-portlet--mobile">

            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right"
                  action="{{url('cashbox/' . $cashbox->cb_id)}}"
                  method="post">
                @method('PATCH')
                {{ csrf_field() }}
                <div class="m-portlet__body">
                    <div class="col-sm-12" style="padding-left: 0;">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @elseif($errors->any())
                            <div class="alert alert-danger">
                                {{ $errors->first() }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group m-form__group">
                        <label for="name">Cash Box Amount</label>
                        <input type="text"
                               class="form-control m-input"
                               id="name"
                               value="{{$cashbox->cb_amountInTheCashBox}}"
                               name="cashBoxAmount"
                               aria-describedby="cashBoxAmount"
                               placeholder="Enter Cash Box Amount">
                        <span class="m-form__help">Cash Box Amount</span>
                    </div>
                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('customjs')
    <script src="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.js')}}"></script>
    <script src="{{asset('public/js/staff.js')}}"></script>
@endsection