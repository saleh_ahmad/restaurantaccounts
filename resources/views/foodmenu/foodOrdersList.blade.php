@extends('layouts.master')

@section('title', 'Food Orders')

@section('customcss')
    <link href="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('main')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Food Orders
                        </h3>
                    </div>
                </div>
                @if (!in_array(Auth::user()->ur_id, [1])):
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{url('food-orders/create')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add New Order</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                @endif
            </div>
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="staffList">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Staff Name</th>
                        <th class="text-center">Total Price</th>
                        <th class="text-center">Discount price</th>
                        <th class="text-center">Final Price</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($foodOrders as $foodOrder)
                        <tr data-id="{{$foodOrder->ofm_id}}">
                            <td>{{$foodOrder->type->oft_name}}</td>
                            <td>{{$foodOrder->staff->user->u_name}}</td>
                            <td class="text-right">{{$foodOrder->ofm_totalPrice}}</td>
                            <td class="text-right">{{$foodOrder->ofm_discountPrice}}</td>
                            <td class="text-right">{{$foodOrder->ofm_finalPrice}}</td>
                            <td class="text-center">
                                <a href="{{url('food-orders/'. $foodOrder->ofm_id)}}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Details">
                                    <i class="la la-search-plus"></i>
                                </a>

                                <a href="{{url('food-orders/'. $foodOrder->ofm_id . '/edit')}}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">
                                    <i class="la la-edit"></i>
                                </a>

                                <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" data-toggle="tooltip"><i class="la la-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('customjs')
    <script src="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/js/staff.js')}}" type="text/javascript"></script>
@endsection