@extends('layouts.master')

@section('title', 'Food Order')

@section('customcss')

@endsection

@section('main')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Food Order
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover" id="food-order">
                    <tr>
                        <th>Date</th>
                        <td>{{$order->created_at}}</td>
                    </tr>
                    <tr>
                        <th>Order Type</th>
                        <td>{{$order->type->oft_name}}</td>
                    </tr>
                    <tr>
                        <th>Staff</th>
                        <td>{{$order->staff->user->u_name}}</td>
                    </tr>
                    <tr>
                        <th>Total price</th>
                        <td>&#2547; {{$order->ofm_totalPrice}}</td>
                    </tr>
                    <tr>
                        <th>Discount Price</th>
                        <td>&#2547; {{$order->ofm_discountPrice}}</td>
                    </tr>
                    <tr>
                        <th>Final Price</th>
                        <td>&#2547; {{$order->ofm_finalPrice}}</td>
                    </tr>
                </table>

                <br />
                <h4>Order Details</h4>
                <table class="table table-striped- table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Menu</th>
                        <th>Quantity</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($order->details as $detail):
                    <tr>
                        <td>{{$detail->foodMenu->fm_name}}</td>
                        <td>{{$detail->ofd_qty}}</td>
                        <td>&#2547; {{$detail->ofd_price}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('customjs')

@endsection