@extends('layouts.master')

@section('title', 'Food Menu')

@section('customcss')
    <link href="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('main')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Food Menu
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{url('staff/create')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add New Menu</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="staffList">
                    <thead>
                    <tr>
                        <th>Category</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th class="text-center">Making Cost</th>
                        <th class="text-center">Selling Price</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($foodMenus as $foodMenu)
                        <tr data-id="{{$foodMenu->fm_id}}">
                            <td>{{$foodMenu->category->fmc_name}}</td>
                            <td>{{$foodMenu->fm_name}}</td>
                            <td>{{$foodMenu->fm_description}}</td>
                            <td class="text-right">{{$foodMenu->fm_makingCost}}</td>
                            <td class="text-right">{{$foodMenu->fm_sellingPrice}}</td>
                            <td class="text-center">
                                <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Details">
                                    <i class="la la-search-plus"></i>
                                </a>

                                <a href="{{url('food-menu/'. $foodMenu->fm_id . '/edit')}}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">
                                    <i class="la la-edit"></i>
                                </a>

                                <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" data-toggle="tooltip"><i class="la la-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('customjs')
    <script src="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/js/staff.js')}}" type="text/javascript"></script>
@endsection