@extends('layouts.master')

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('title', 'New Food Order')

@section('customcss')
    <link href="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('main')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">New Food Order</h3>
                </div>
            </div>
        </div>

        <div class="m-portlet m-portlet--mobile">

            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right"
                  id="food-order-form"
                  action="{{url('food-orders')}}"
                  method="post">

                <div class="col-sm-12" id="add-food-alert" style="padding-left: 0;">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @elseif($errors->any())
                        <div class="alert alert-danger">
                            {{ $errors->first() }}
                        </div>
                    @endif
                </div>

                {{ csrf_field() }}
                <div class="m-portlet__body">
                    <div class="form-group m-form__group">
                        <label for="type">Order Type</label>
                        <select id="type" name="type" class="form-control m-input">
                            @foreach ($types as $type):
                            <option value="{{$type->oft_id}}">{{$type->oft_name}}</option>
                            @endforeach
                        </select>
                        <span class="m-form__help">Choose Order Type</span>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="food">Select Food</label>
                        <select id="food" name="food" class="form-control m-input">
                            <option></option>
                            @foreach ($foodMenus as $foodMenu):
                            <option value="{{$foodMenu->fm_id}}">{{$foodMenu->fm_name}}</option>
                            @endforeach
                        </select>
                        <span class="m-form__help">Choose Food from the Menu</span>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="quantity">Quantity</label>
                        <input type="number"
                               class="form-control m-input"
                               id="quantity"
                               name="quantity"
                               value="1"
                               aria-describedby="quantity"
                               placeholder="Enter Quantity">
                        <span class="m-form__help">Enter Quantity</span>
                    </div>

                    <div class="m-form__actions">
                        <button type="button" id="addFood" class="btn btn-info">Add</button>
                    </div>

                    <br />

                    <div class="m-form__actions">
                        <table class="table table-bordered" id="food-orders-table">
                            <thead>
                            <tr>
                                <th class="text-center">Menu</th>
                                <th class="text-center">Unit Price</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody id="ordered-foods">
                            </tbody>
                        </table>
                    </div>

                    <br />

                    <div class="form-group m-form__group">
                        <label for="totalPrice">Total Price</label>
                        <input type="number" class="form-control m-input" value="0.00" id="totalPrice" name="totalPrice" aria-describedby="totalPrice" readonly>
                        <span class="m-form__help">Total Price</span>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="discount">Discount</label>
                        <input type="number" class="form-control m-input" value="0.00" id="discount" name="discount" aria-describedby="totalPrice">
                        <span class="m-form__help">Total Price</span>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="finalPrice">Final Price Price</label>
                        <input type="number" class="form-control m-input" value="0.00" id="finalPrice" name="finalPrice" aria-describedby="totalPrice">
                        <span class="m-form__help">Final Price</span>
                    </div>
                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-primary">Place Order</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('customjs')
    <script src="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.js')}}"></script>
    <script src="{{asset('public/js/addFoodOrder.js')}}"></script>
@endsection