@extends('layouts.master')

@section('title', 'Add New Staff')

@section('customcss')
    <link href="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('main')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">New Staff</h3>
                </div>
            </div>
        </div>

        <div class="m-portlet m-portlet--mobile">

            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right"
                  action="{{url('staff')}}"
                  method="post"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="m-portlet__body">
                    <div class="col-sm-12" style="padding-left: 0;">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @elseif($errors->any())
                            <div class="alert alert-danger">
                                {{ $errors->first() }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group m-form__group">
                        <label for="name">Name</label>
                        <input type="text"
                               class="form-control m-input"
                               id="name" value="{{old('name')}}"
                               name="name"
                               aria-describedby="staffName"
                               placeholder="Enter name">
                        <span class="m-form__help">Staff's Full Name</span>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="mobile">Mobile</label>
                        <input type="text"
                               class="form-control m-input"
                               id="mobile"
                               value="{{old('mobile')}}"
                               name="mobile"
                               aria-describedby="staffMobile"
                               placeholder="Enter Mobile No.">
                        <span class="m-form__help">Staff's Contact Number</span>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="address">Address</label>
                        <textarea class="form-control m-input" id="address" name="address" rows="3" placeholder="Enter Address">{{old('address')}}</textarea>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control m-input" id="password" name="password" placeholder="Choose a password for the staff">
                    </div>

                    <div class="form-group m-form__group">
                        <label for="role">Role</label>
                        <select id="role" name="role" class="form-control m-input">
                            @foreach ($userRoles as $userRole):
                            <option value="{{$userRole->ur_id}}">{{$userRole->ur_name}}</option>
                            @endforeach
                        </select>
                        <span class="m-form__help">Choose Staff Role</span>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="image">Image</label>
                        <input type="file" class="form-control m-input" id="image" name="image" aria-describedby="emailHelp">
                        <span class="m-form__help">Staff's Picture</span>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="salary">Salary</label>
                        <input type="text" class="form-control m-input" id="salary" name="salary" value="{{old('salary')}}" aria-describedby="emailHelp" placeholder="Enter Salary">
                        <span class="m-form__help">Staff's Salary you'll provide</span>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="joiningDate">Joining Date</label>
                        <input type="text" class="form-control m-input" id="joiningDate" name="joiningDate" value="{{old('joiningDate')}}" aria-describedby="emailHelp" placeholder="Enter Staff's joining date">
                        <span class="m-form__help">Staff's Joining Date</span>
                    </div>
                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-primary">Add Staff</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('customjs')
    <script src="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.js')}}"></script>
    <script src="{{asset('public/js/staff.js')}}"></script>
@endsection