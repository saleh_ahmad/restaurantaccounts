@extends('layouts.master')

@section('title', 'Dashboard')

@section('customcss')
@endsection

@section('main')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title ">Dashboard</h3>
                </div>
            </div>
        </div>

        <!-- END: Subheader -->
        <div class="m-content">

            <!--Begin::Section-->
            <div class="row">
                <div class="col-xl-12">

                    <!--begin:: Widgets/Quick Stats-->
                    <div class="row m-row--full-height">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="m-portlet m-portlet--half-height m-portlet--border-bottom-brand ">
                                <div class="m-portlet__body">
                                    <div class="m-widget26">
                                        <div class="m-widget26__number">
                                            {{$todayTotalSell}}
                                            <small>Total Sells Today</small>
                                        </div>
                                        <div class="m-widget26__chart" style="height:90px; width: 220px;">
                                            <canvas id="m_chart_quick_stats_1"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m--space-30"></div>
                            <div class="m-portlet m-portlet--half-height m-portlet--border-bottom-danger ">
                                <div class="m-portlet__body">
                                    <div class="m-widget26">
                                        <div class="m-widget26__number">
                                            {{$todayTotalFoodOrders}}
                                            <small>Total Food Orders Today</small>
                                        </div>
                                        <div class="m-widget26__chart" style="height:90px; width: 220px;">
                                            <canvas id="m_chart_quick_stats_2"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="m-portlet m-portlet--half-height m-portlet--border-bottom-success ">
                                <div class="m-portlet__body">
                                    <div class="m-widget26">
                                        <div class="m-widget26__number">
                                            {{$todayInventoryCosts}}
                                            <small>Total Costs Today</small>
                                        </div>
                                        <div class="m-widget26__chart" style="height:90px; width: 220px;">
                                            <canvas id="m_chart_quick_stats_3"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m--space-30"></div>
                            <div class="m-portlet m-portlet--half-height m-portlet--border-bottom-accent ">
                                <div class="m-portlet__body">
                                    <div class="m-widget26">
                                        <div class="m-widget26__number">
                                            {{$totalSellMonthly}}
                                            <small>Total Sells in this month</small>
                                        </div>
                                        <div class="m-widget26__chart" style="height:90px; width: 220px;">
                                            <canvas id="m_chart_quick_stats_4"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end:: Widgets/Quick Stats-->
                </div>
            </div>
            <!--End::Section-->
        </div>
    </div>
@endsection

@section('customjs')
    <script src="{{asset('public/assets/app/js/dashboard.js')}}" type="text/javascript"></script>
@endsection