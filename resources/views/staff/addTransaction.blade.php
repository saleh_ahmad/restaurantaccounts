@extends('layouts.master')

@section('title', 'Add Salary Transaction')

@section('customcss')
<link href="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('main')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">New Salary transaction</h3>
            </div>
        </div>
    </div>

    <div class="m-portlet m-portlet--mobile">

        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right"
              action="{{url('salary-transaction')}}"
              method="post"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="m-portlet__body">
                <div class="col-sm-12" style="padding-left: 0;">
                    @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @elseif($errors->any())
                    <div class="alert alert-danger">
                        {{ $errors->first() }}
                    </div>
                    @endif
                </div>

                <div class="form-group m-form__group">
                    <label for="staff">Staff</label>
                    <select id="staff" name="staff" class="form-control m-input">
                        @foreach ($staffList as $staff):
                        <option value="{{$staff->s_id}}">{{$staff->user->u_name . ' - ' . $staff->user->userRole->ur_name}}</option>
                        @endforeach
                    </select>
                    <span class="m-form__help">Choose Staff</span>
                </div>

                <div class="form-group m-form__group">
                    <label for="salary">Salary Amount</label>
                    <input type="text" class="form-control m-input" id="salary" name="salary" value="{{old('salary')}}" aria-describedby="emailHelp" placeholder="Enter Salary">
                    <span class="m-form__help">Staff's Salary you'll provide</span>
                </div>

                <div class="form-group m-form__group">
                    <label for="month">Select Month</label>
                    <input type="text" class="form-control m-input" id="month" name="month" value="{{old('month')}}" aria-describedby="emailHelp" placeholder="Month of the Salary">
                    <span class="m-form__help">Month of the salary</span>
                </div>
            </div>

            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <button type="submit" class="btn btn-primary">Add</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('customjs')
<script src="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('public/js/salary.transaction.js')}}"></script>
@endsection