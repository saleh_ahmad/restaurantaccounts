@extends('layouts.master')

@section('title', 'Salary Transactions')

@section('customcss')
    <link href="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('main')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Salary Transactions
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{url('salary-transaction/create')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add New Transaction</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="staffList">
                    <thead>
                    <tr>
                        <th>Staff</th>
                        <th>Role</th>
                        <th class="text-center">Given Salary</th>
                        <th class="text-center">Paid Salary</th>
                        <th class="text-center">Due Salary</th>
                        <th>Owner</th>
                        <th>Month</th>
                        <th>Given Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($transactions as $transaction)
                        <tr data-id="{{$transaction->st_id}}">
                            <td>{{$transaction->staff->user->u_name}}</td>
                            <td>{{$transaction->staff->user->userRole->ur_name}}</td>
                            <td class="text-right">{{number_format($transaction->st_givenSalary, 2)}}</td>
                            <td class="text-right">{{number_format($transaction->st_totalPaidSalary, 2)}}</td>
                            <td class="text-right">{{number_format($transaction->st_totalDueSalary, 2)}}</td>
                            <td>{{$transaction->owner->user->u_name}}</td>
                            <td>{{$transaction->st_month}}</td>
                            <td>{{$transaction->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('customjs')
    <script src="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/js/staff.js')}}" type="text/javascript"></script>
@endsection