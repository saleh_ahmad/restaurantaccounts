@extends('layouts.master')

@section('title', 'Inventory Status')

@section('customcss')
    <link href="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('main')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{$inventory[0]->inv_date}}
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="inventoryStatus">
                    <thead>
                    <tr>
                        <th>Raw Material</th>
                        <th>Beginning Quantity</th>
                        <th>Ordered Quantity</th>
                        <th>Received Quantity</th>
                        <th>Present Quantity</th>
                        <th>Ending Quantity</th>
                        <th>Usage</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($inventory as $inv)
                        <tr>
                            <td>{{$inv->rawMaterial->rm_name}}</td>
                            <td>{{$inv->Inv_beginningQty}}</td>
                            <td>{{$inv->Inv_orderedQty}}</td>
                            <td>{{$inv->Inv_rerceivedQty}}</td>
                            <td>{{$inv->inv_presentQty}}</td>
                            <td>{{$inv->inv_endingQty}}</td>
                            <td>{{$inv->inv_usage}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('customjs')
    <script src="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/js/staff.js')}}" type="text/javascript"></script>
@endsection