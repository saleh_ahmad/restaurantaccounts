@extends('layouts.master')

@section('title', 'Inventory Orders')

@section('customcss')
    <link href="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('main')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Inventory Orders
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{url('inventiry-orders/create')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Inventory Order</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable" id="staffList">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Vendor</th>
                        <th>Bill</th>
                        <th>Paid</th>
                        <th>Ordered</th>
                        <th>Received</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($inventoryOrders as $inventoryOrder)
                        <tr data-id="{{$inventoryOrder->iom_id}}">
                            <td>{{$inventoryOrder->iom_date}}</td>
                            <td>{{$inventoryOrder->vendor->v_name}}</td>
                            <td>{{$inventoryOrder->iom_bill}}</td>
                            <td>{{$inventoryOrder->iom_paid}}</td>
                            <td>{{$inventoryOrder->orderedUser->u_name}}</td>
                            <td>{{$inventoryOrder->receivedUser->u_name}}</td>
                            <td>
                                <a href="{{url('inventory-orders/'. $inventoryOrder->iom_id)}}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Details">
                                    <i class="la la-search-plus"></i>
                                </a>

                                <a href="{{url('inventory-orders/'. $inventoryOrder->iom_id . '/edit')}}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">
                                    <i class="la la-edit"></i>
                                </a>

                                <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" data-toggle="tooltip"><i class="la la-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('customjs')
    <script src="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/js/staff.js')}}" type="text/javascript"></script>
@endsection