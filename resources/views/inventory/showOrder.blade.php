@extends('layouts.master')

@section('title', 'Inventory Order')

@section('customcss')

@endsection

@section('main')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Inventory Order
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">

                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover" id="inventory-order">
                    <tr>
                        <th>Date</th>
                        <td>{{$order->iom_date}}</td>
                    </tr>
                    <tr>
                        <th>Vendor</th>
                        <td>{{$order->vendor->v_name}}</td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td>{{$order->iom_description}}</td>
                    </tr>
                    <tr>
                        <th>Bill</th>
                        <td>&#2547; {{$order->iom_bill}}</td>
                    </tr>
                    <tr>
                        <th>Paid</th>
                        <td>&#2547; {{$order->iom_paid}}</td>
                    </tr>
                    <tr>
                        <th>Ordered By</th>
                        <td>{{$order->orderedUser->u_name}}</td>
                    </tr>
                    <tr>
                        <th>Received By</th>
                        <td>{{$order->receivedUser->u_name}}</td>
                    </tr>
                </table>

                <br />
                <h4>Order Details</h4>
                <table class="table table-striped- table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Raw Material</th>
                        <th>Unit Price</th>
                        <th>Ordered Quantity</th>
                        <th>Received Quantity</th>
                        <th>Total Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($order->details as $detail):
                    <tr>
                        <td>{{$detail->rawMaterial->rm_name}}</td>
                        <td>&#2547; {{$detail->iod_unitPrice}}</td>
                        <td>{{$detail->iod_orderedQuantity}}</td>
                        <td>{{$detail->iod_receivedQuantity}}</td>
                        <td>&#2547; {{$detail->iod_totalPrice}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('customjs')

@endsection