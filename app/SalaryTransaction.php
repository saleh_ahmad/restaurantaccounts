<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalaryTransaction extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'salarytransaction';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'st_id';

    /**
     * Get Staff Salary Month.
     *
     * @param  string  $value
     * @return string
     */
    public function getStMonthAttribute($date)
    {
        return $date ? Carbon::createFromFormat('Y-m-d', $date)->format('F, Y') : '';
    }

    /**
     * Get Staff Salary Transaction date.
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($date)
    {
        return $date ? Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('j F, Y') : '';
    }

    /**
     * Get the staff that owns the Salary Transaction.
     */
    public function staff()
    {
        return $this->belongsTo('App\Staff', 's_id', 's_id');
    }

    /**
     * Get the owner that owns the Salary Transaction.
     */
    public function owner()
    {
        return $this->belongsTo('App\Owner', 'o_id', 'o_id');
    }
}
