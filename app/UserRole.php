<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRole extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'userroles';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ur_id';

    /**
     * Get the Users for the User Role.
     */
    public function user()
    {
        return $this->hasMany('App\User', 'ur_id', 'ur_id');
    }
}
