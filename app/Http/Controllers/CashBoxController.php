<?php

namespace App\Http\Controllers;

use App\CashBox;
use App\CashBoxTrnx;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CashBoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = 'cashbox';

        $today      = Carbon::today()->toDateString();
        $totalDebit = CashBoxTrnx::where('cbt_type', 1)
            ->whereDate('created_at', $today)
            ->sum('cbt_amount');

        $totalCredit = CashBoxTrnx::where('cbt_type', 2)
            ->whereDate('created_at', $today)
            ->sum('cbt_amount');

        $expectedAmount = $totalDebit - $totalCredit;

        $cashbox = CashBox::whereDate('cb_date', $today)->first();

        if (!$cashbox) {
            $cashbox = new CashBox;

            $cashbox->cb_date = $today;
        }

        $cashbox->cb_totalDebit     = $totalDebit;
        $cashbox->cb_totalCredit    = $totalCredit;
        $cashbox->cb_expectedAmount = $expectedAmount;

        $cashbox->save();

        $data['status'] = $cashbox;

        return view('cashbox.status', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu'] = 'cashbox';
        $data['cashbox'] = CashBox::find($id);

        return view('cashbox.editCashBox', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'cashBoxAmount' => 'required|numeric|min:0'
        ];

        $messages = [
            'cashBoxAmount.required' => 'Please provide amount in the Cash Box.',
            'cashBoxAmount.numeric'  => 'Please provide a valid amount in the Cash Box.',
            'cashBoxAmount.min'      => 'Amount in the cashbox can\'t be less than 0.'
        ];

        $this->validate($request, $rules, $messages);

        $amount = $request->input('cashBoxAmount');

        $cashbox = CashBox::find($id);

        $cashbox->cb_amountInTheCashBox = $amount;

        $cashbox->save();

        return back()
            ->with('success', 'Amount in the Cash Box has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
