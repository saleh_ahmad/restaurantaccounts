<?php

namespace App\Http\Controllers;

use App\InventoryOrderMaster;
use App\OrderedFoodsMaster;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['menu'] = 'dashboard';

        $today     = Carbon::today()->toDateString();
        $thisMonth = Carbon::today()->format('m');
        $thisYear  = Carbon::today()->format('Y');

        $data['todayTotalSell']       = OrderedFoodsMaster::whereDate('created_at', $today)->sum('ofm_finalPrice');
        $data['totalSellMonthly']     = OrderedFoodsMaster::whereMonth('created_at', $thisMonth)
            ->whereYear('created_at', $thisYear)
            ->sum('ofm_finalPrice');
        $data['todayInventoryCosts']  = InventoryOrderMaster::whereDate('iom_date', $today)->sum('iom_bill');
        $data['todayTotalFoodOrders'] = OrderedFoodsMaster::whereDate('created_at', $today)->count();

        return view('dashboard', $data);
    }
}
