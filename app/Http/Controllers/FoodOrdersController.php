<?php

namespace App\Http\Controllers;

use App\CashBoxTrnx;
use App\FoodMenu;
use App\OrderedFoodsDetails;
use App\OrderedFoodsMaster;
use App\OrderedFoodsType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use test\Mockery\MockingVariadicArgumentsTest;

class FoodOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = 'foods';
        $data['foodOrders'] = OrderedFoodsMaster::all();

        return view('foodmenu.foodOrdersList', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = 'foods';
        $data['types'] = OrderedFoodsType::all();
        $data['foodMenus'] = FoodMenu::all();

        return view('foodmenu.addFoodOrder', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type     = $request->input('type');
        $foods    = $request->input('food');
        $qty      = $request->input('qty');
        $discount = $request->input('discount');

        $orderedFoodMaster = new OrderedFoodsMaster;

        $orderedFoodMaster->oft_id            = $type;
        $orderedFoodMaster->ofm_discountPrice = $discount;
        $orderedFoodMaster->s_id              = Auth::user()->staff->s_id;

        $orderedFoodMaster->save();

        $finalPrice = 0.00;
        foreach ($foods as $i => $food) {
            $price = FoodMenu::select('fm_sellingPrice')
                ->where('fm_id', $food)
                ->first()
                ->fm_sellingPrice;
            $quantity = $qty[$i];
            $total = $price * $quantity;

            $finalPrice += $total;

            $orderFoodDetails = new OrderedFoodsDetails;

            $orderFoodDetails->ofm_id    = $orderedFoodMaster->ofm_id;
            $orderFoodDetails->fm_id     = $food;
            $orderFoodDetails->ofd_qty   = $quantity;
            $orderFoodDetails->ofd_price = $total;

            $orderFoodDetails->save();
        }

        $orderedFoodMaster->ofm_totalPrice = $finalPrice;

        $finalPrice -= $discount;

        $orderedFoodMaster->ofm_finalPrice = $finalPrice;

        $orderedFoodMaster->save();

        $cashBoxTrnx = new CashBoxTrnx;

        $cashBoxTrnx->cbt_type    = 1;
        $cashBoxTrnx->cbt_amount  = $finalPrice;
        $cashBoxTrnx->cbt_purpose = 'Customer Order';
        $cashBoxTrnx->cbtc_id     = 3;
        $cashBoxTrnx->u_id        = Auth::id();

        $cashBoxTrnx->save();

        return back()
            ->with('success', 'One Order has been taken successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['menu'] = 'foods';
        $data['order'] = OrderedFoodsMaster::find($id);

        return view('foodmenu.showFoodOrder', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchFoodPrice(Request $request)
    {
        $response = [
            'error'   => false,
            'success' => false
        ];

        $id = $request->input('id');

        $response['food'] = $this->foodPrice($id);

        $response['success'] = true;
        return response()
            ->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * @param $id
     * @return mixed
     */
    private function foodPrice($id)
    {
        return FoodMenu::select('fm_id', 'fm_name', 'fm_sellingPrice as price')
            ->where('fm_id', $id)
            ->first();
    }
}
