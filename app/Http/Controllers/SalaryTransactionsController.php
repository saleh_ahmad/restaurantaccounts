<?php

namespace App\Http\Controllers;

use App\SalaryTransaction;
use App\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SalaryTransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = 'staff';
        $data['transactions'] = SalaryTransaction::all();

        /** Rendering view for salary transactions */
        return view('staff.salaryTransactions', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = 'staff';
        $data['staffList'] = Staff::where('s_endingDate', null)
            ->get();

        /** Rendering view for staff List */
        return view('staff.addTransaction', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'staff'  => 'required|integer',
            'salary' => 'required|numeric',
            'month'  => 'required'
        ];

        $messages = [
            'staff.required'  => 'Please select a Staff.',
            'staff.integer'   => 'Please select a valid Staff',
            'salary.required' => 'Please provide Staff Salary Amount',
            'salary.numeric'  => 'Please provide a valid Salary Amount.',
            'month.required'  => 'Please select a month'
        ];

        $this
            ->validate($request, $rules, $messages);

        $salary = $request->input('salary');
        $staff  = $request->input('staff');
        $month  = Carbon::createFromFormat('m-Y', $request->input('month'))
                ->format('Y-m') . '-01';

        /**
         * Insert into `users`
         */
        $salaryTransaction = new SalaryTransaction;

        $salaryTransaction->st_givenSalary = $salary;
        $salaryTransaction->s_id           = $staff;
        $salaryTransaction->o_id           = Auth::user()->owner->o_id;
        $salaryTransaction->st_month       = $month;

        $salaryTransaction->save();

        $staffSalary = Staff::find(1)->s_salary;

        $totalpaidAmount = SalaryTransaction::where('s_id', $staff)
            ->where('st_month', $month)
            ->sum('st_givenSalary');

        $due = $staffSalary - $totalpaidAmount;

        $salaryTransaction->st_totalPaidSalary = $totalpaidAmount;
        $salaryTransaction->st_totalDueSalary  = $due;

        $salaryTransaction->save();

        return back()
            ->with('success', 'Transaction Successful.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
