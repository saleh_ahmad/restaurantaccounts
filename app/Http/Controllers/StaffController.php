<?php

namespace App\Http\Controllers;

use App\Staff;
use App\User;
use App\UserRole;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = 'staff';
        $data['staffList'] = Staff::where('s_endingDate', null)
            ->get();

        /** Rendering view for staff List */
        return view('staff.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = 'staff';
        $data['userRoles'] = UserRole::where('ur_id', '<>', 1)
            ->get();

        /** Rendering view for staff List */
        return view('staff.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'        => 'required|string',
            'mobile'      => 'required|unique:users,u_mobile',
            'address'     => 'nullable',
            'role'        => 'required|integer',
            'password'    => 'required|min:6',
            'image'       => 'nullable|file|image',
            'salary'      => 'required|numeric|min:0',
            'joiningDate' => 'required'
        ];

        $messages = [
            'name.required'        => 'Please provide Staff Name.',
            'name.string'          => 'Please provide a valid Staff Name.',
            'mobile.required'      => 'Please provide staff\'s mobile no..',
            'mobile.unique'        => 'This mobile number has already used.',
            'password.required'    => 'Please provide a password for the staff.',
            'password.min'         => 'Password must contain minimum 6 characters.',
            'image.file'           => 'Please provide an image file.',
            'image.image'          => 'Please provide an image file.',
            'role.required'        => 'Please select Staff Role',
            'role.integer'         => 'Please select a valid Staff Role',
            'salary.required'      => 'Please provide Staff Salary Amount',
            'salary.numeric'       => 'Please provide a valid Salary Amount.',
            'salary.min'           => 'Salary can\'t be less than 0.',
            'joiningDate.required' => 'Please provide the joining date.'
        ];

        $this
            ->validate($request, $rules, $messages);

        /**
         * Insert into `users`
         */
        $user = new User;

        $user->u_name     = $request->input('name');
        $user->u_mobile   = $request->input('mobile');
        $user->u_password = Hash::make($request->input('password'));
        $user->u_address  = $request->input('address');
        $user->ur_id      = $request->input('role');

        $user->save();

        $user->u_image = '';
        $file = $request->file('image');

        if ($file) {
            $file->store('users');
            $user->u_image = 'storage/app/users/' . $file->hashName();

            $user->save();
        }

        $staff = new Staff;

        $staff->u_id          = $user->u_id;
        $staff->s_salary      = $request->input('salary');
        $staff->s_joiningDate = Carbon::createFromFormat('d/m/Y', $request->input('joiningDate'))
            ->format('Y-m-d');

        $staff->save();

        return back()
            ->with('success', 'A Staff has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
