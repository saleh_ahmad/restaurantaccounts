<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'staff';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 's_id';

    /**
     * Get Staff joining date.
     *
     * @param  string  $value
     * @return string
     */
    public function getSJoiningDateAttribute($date)
    {
        return $date ? Carbon::createFromFormat('Y-m-d', $date)->format('j F, Y') : '';
    }

    /**
     * Get the user that owns the staff.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'u_id', 'u_id');
    }

    /**
     * Get the Salary Transactions for the staff.
     */
    public function salaryTransaction()
    {
        return $this->hasMany('App\SalaryTransaction', 's_id', 's_id');
    }

    /**
     * Get the Ordered Foods for the Staff.
     */
    public function orderedFoods()
    {
        return $this->hasMany('App\OrderedFoodsMaster', 's_id', 's_id');
    }
}
