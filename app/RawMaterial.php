<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RawMaterial extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rawmaterials';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'rm_id';

    /**
     * Get the Inventory Status for the raw material.
     */
    public function inventory()
    {
        return $this->hasMany('App\Inventory', 'rm_id', 'rm_id');
    }

    /**
     * Get the Inventory Order Details for the raw material.
     */
    public function inventoryOrderDetails()
    {
        return $this->hasMany('App\InventoryOrderDetails', 'rm_id', 'rm_id');
    }
}
