<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashBox extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cashbox';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'cb_id';

    /**
     * Get Staff joining date.
     *
     * @param  string  $value
     * @return string
     */
    public function getCbDateAttribute($date)
    {
        return $date ? Carbon::createFromFormat('Y-m-d', $date)->format('j F, Y') : '';
    }
}
