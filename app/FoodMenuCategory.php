<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodMenuCategory extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'foodmenucategories';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'fmc_id';

    /**
     * Get the Food Menus for the Category.
     */
    public function foodMenus()
    {
        return $this->hasMany('App\FoodMenu', 'fmc_id', 'fmc_id');
    }
}
