<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryOrderMaster extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inventoryordermaster';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'iom_id';

    /**
     * Get Staff Inventory Order Date.
     *
     * @param  string  $value
     * @return string
     */
    public function getIomDateAttribute($date)
    {
        return $date ? Carbon::createFromFormat('Y-m-d', $date)->format('j F, Y') : '';
    }

    /**
     * Get the Inventory Order Details for the Inventory Order Master.
     */
    public function details()
    {
        return $this->hasMany('App\InventoryOrderDetails', 'iom_id', 'iom_id');
    }

    /**
     * Get the vendor that owns the Inventory Order Master.
     */
    public function vendor()
    {
        return $this->belongsTo('App\Vendor', 'v_id', 'v_id');
    }

    /**
     * Get the Ordered user that owns the Inventory Order Master.
     */
    public function orderedUser()
    {
        return $this->belongsTo('App\User', 'iom_orderedUserId', 'u_id');
    }

    /**
     * Get the Received user that owns the Inventory Order Master.
     */
    public function receivedUser()
    {
        return $this->belongsTo('App\User', 'iom_receivedUserId', 'u_id');
    }
}
