<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodMenu extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'foodmenus';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'fm_id';

    /**
     * Get the category that owns the food menu.
     */
    public function category()
    {
        return $this->belongsTo('App\FoodMenuCategory', 'fmc_id', 'fmc_id');
    }

    /**
     * Get the Food Menus Images for the Food Menu.
     */
    public function foodMenuImages()
    {
        return $this->hasMany('App\FoodMenuImages', 'fm_id', 'fm_id');
    }

    /**
     * Get the Ordered Foods Details for the Food Menu.
     */
    public function orderedFoodsDetails()
    {
        return $this->hasMany('App\OrderedFoodsDetails', 'fm_id', 'fm_id');
    }
}
