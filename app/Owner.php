<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Owner extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'owners';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'o_id';

    /**
     * Get the user that owns the owner.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'u_id', 'u_id');
    }

    /**
     * Get the Salary Transactions for the owner.
     */
    public function salaryTransaction()
    {
        return $this->hasMany('App\SalaryTransaction', 'o_id', 'o_id');
    }
}
