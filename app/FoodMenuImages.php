<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodMenuImages extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'foodmenuimages';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'fmi_id';

    /**
     * Get the Food Menu that owns the food Menu Image.
     */
    public function foodMenu()
    {
        return $this->belongsTo('App\FoodMenu', 'fm_id', 'fm_id');
    }
}
