<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderedFoodsType extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orderedfoodstypes';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'oft_id';

    /**
     * Get the Ordered Foods for the Ordered Food type.
     */
    public function details()
    {
        return $this->hasMany('App\OrderedFoodsMaster', 'oft_id', 'oft_id');
    }
}
