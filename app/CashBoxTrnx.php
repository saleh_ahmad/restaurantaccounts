<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashBoxTrnx extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cashboxtrnx';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'cbt_id';

    /**
     * Get Staff Transaction Type.
     *
     * @param  string  $value
     * @return string
     */
    public function getCbtTypeAttribute($type)
    {
        return $type == 1 ? 'Debit' : 'Credit';
    }

    /**
     * Get the Cash Box Transaction Category that owns the Cash Box Transaction.
     */
    public function category()
    {
        return $this->belongsTo('App\CashBoxTrnxCategory', 'cbtc_id', 'cbtc_id');
    }
}
