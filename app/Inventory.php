<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inventory';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'inv_id';

    /**
     * Get Staff joining date.
     *
     * @param  string  $value
     * @return string
     */
    public function getInvDateAttribute($date)
    {
        return $date ? Carbon::createFromFormat('Y-m-d', $date)->format('j F, Y') : '';
    }

    /**
     * Get the Raw Materials that owns the Inventory.
     */
    public function rawMaterial()
    {
        return $this->belongsTo('App\RawMaterial', 'rm_id', 'rm_id');
    }
}
