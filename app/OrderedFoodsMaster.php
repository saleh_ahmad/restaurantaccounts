<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderedFoodsMaster extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orderedfoodsmaster';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ofm_id';

    /**
     * Get Staff joining date.
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($date)
    {
        return $date ? Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('j F, Y; g:i A') : '';
    }

    /**
     * Get the Ordered Food Details for the Ordered Food Master.
     */
    public function details()
    {
        return $this->hasMany('App\OrderedFoodsDetails', 'ofm_id', 'ofm_id');
    }

    /**
     * Get the Ordered Food Type that owns the Ordered Food Master.
     */
    public function type()
    {
        return $this->belongsTo('App\OrderedFoodsType', 'oft_id', 'oft_id');
    }

    /**
     * Get the Staff that owns the Ordered Food Master.
     */
    public function staff()
    {
        return $this->belongsTo('App\Staff', 's_id', 's_id');
    }
}
