<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'u_name', 'u_mobile', 'u_password',
    ];

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'u_id';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'u_password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the owner associated with the users.
     */
    public function owner()
    {
        return $this->hasOne('App\Owner', 'u_id', 'u_id');
    }

    /**
     * Get the staff associated with the users.
     */
    public function staff()
    {
        return $this->hasOne('App\Staff', 'u_id', 'u_id');
    }

    /**
     * Get the User Role that owns the user.
     */
    public function userRole()
    {
        return $this->belongsTo('App\UserRole', 'ur_id', 'ur_id');
    }
}
