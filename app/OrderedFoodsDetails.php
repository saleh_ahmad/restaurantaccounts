<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderedFoodsDetails extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orderedfoodsdetails';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'ofd_id';

    /**
     * Get the Ordered Food master that owns the Ordered Food Details.
     */
    public function master()
    {
        return $this->belongsTo('App\OrderedFoodsMaster', 'ofm_id', 'ofm_id');
    }

    /**
     * Get the Food Menu that owns the ordered Food Details.
     */
    public function foodMenu()
    {
        return $this->belongsTo('App\FoodMenu', 'fm_id', 'fm_id');
    }
}
