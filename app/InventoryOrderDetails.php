<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryOrderDetails extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inventoryorderdetails';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'iod_id';

    /**
     * Get the Inventory Order master that owns the Inventory Order Details.
     */
    public function master()
    {
        return $this->belongsTo('App\InventoryOrderMaster', 'iom_id', 'iom_id');
    }

    /**
     * Get the Inventory Order master that owns the Inventory Order Details.
     */
    public function rawMaterial()
    {
        return $this->belongsTo('App\RawMaterial', 'rm_id', 'rm_id');
    }
}
