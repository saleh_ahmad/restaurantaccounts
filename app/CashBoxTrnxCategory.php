<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashBoxTrnxCategory extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cashboxtrnxcategories';

    /**
     * The Primary id associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'cbtc_id';

    /**
     * Get the Cash Box Transactions for the Cash Box Transaction Catergories.
     */
    public function trnx()
    {
        return $this->hasMany('App\CashBoxTrnx', 'cbtc_id', 'cbtc_id');
    }
}
